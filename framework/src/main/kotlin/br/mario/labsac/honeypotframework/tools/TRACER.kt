package br.mario.labsac.honeypotframework.tools

import android.content.Context
import android.content.res.Resources.NotFoundException

import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.DEBUG
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.ERROR
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.INFO
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.WARN

import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug
import org.jetbrains.anko.error
import org.jetbrains.anko.info
import org.jetbrains.anko.toast
import org.jetbrains.anko.warn

/** Created by MarioH on 06/03/2018.  */
class TRACER(TAG: String, val con: Context, private val toastError: Boolean = false) {
	enum class LOGTYPE {
		INFO, DEBUG, WARN, ERROR
	}

	private val logger = AnkoLogger(TAG)

	/**
	 * Write a message to the log tracer
	 *
	 * @param type Message type INFO, DEBUG, ERROR, WARN
	 * @param message Message content
	 */
	fun log(type: LOGTYPE, message: String, eRRor: Exception? = null) {
		when (type) {
			INFO -> logger.info { message }
			DEBUG -> logger.debug { message }
			WARN -> logger.warn { message }
			ERROR -> {
				eRRor?.let { logger.error(message, eRRor) } ?: logger.error { message }
				if (toastError)
					con.toast(message)
			}
		}
	}

	/**
	 * Try to get a string resource. Otherwise, show the message number.
	 *
	 * @param resourceId Resource ID
	 * @param sit Message number to show
	 * @return Message text
	 */
	fun prop(resourceId: Int, sit: Int): String {
		return (
			try {
				con.getString(resourceId)

			} catch (rnf: NotFoundException) {
				("HP message situation: $sit")
			}
		)
	}
}