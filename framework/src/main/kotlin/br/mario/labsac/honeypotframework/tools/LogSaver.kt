package br.mario.labsac.honeypotframework.tools

import br.mario.labsac.honeypotframework.R
import br.mario.labsac.honeypotframework.app
import br.mario.labsac.honeypotframework.appHoney

import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking

import java.io.File
import java.io.FileOutputStream
import java.io.IOException

private const val TAG = "FILE SAVER"

/**
 * Responsible for save the services' logs in the private application's folder
 *
 * @author Mário Henrique
 * Created on 09/03/2018.
 */
class LogSaver
constructor(archiveName: String, trailString: String) : Tracing {
	private val logger = TRACER(TAG, app, false)
	override val logTag: String = TAG

	init {
		// logger.log(INFO, "$TAG!!")
		info("$TAG!!")

		val path = File(app.filesDir, app.getString(R.string.log_folder))
		if (!path.exists()) path.mkdir()

		val out = FileOutputStream(File(path, "$archiveName.log"), true)

		// logger.log(INFO, "Archive path: ${path.absolutePath}/$archiveName")
		info("Archive path: ${path.absolutePath}/$archiveName")

		runBlocking {
			launch(context) {
				// out.use({logger.log(ERROR, logger.prop(R.string.HPP_ioExceptionWritingLog, 44), it)}) {
				// it.write("$trailString\n".toByteArray()) }
				try {
					out.write("$trailString\n".toByteArray())
					out.close()

				} catch (ioe: IOException) {
					// logger.log(ERROR, logger.prop(R.string.HPP_ioExceptionWritingLog, 44), ioe)
					error(appHoney.prop(R.string.HPP_ioExceptionWritingLog), 51, ioe)
				}
			}
		}
	}
}