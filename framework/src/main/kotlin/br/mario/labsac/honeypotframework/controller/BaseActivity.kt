package br.mario.labsac.honeypotframework.controller

import android.os.Bundle
import android.os.StrictMode

import android.support.v7.app.AppCompatActivity

import br.mario.labsac.honeypotframework.serviceBasis.ServiceImage
import br.mario.labsac.honeypotframework.tools.TRACER
import br.mario.labsac.honeypotframework.tools.Tracing

/** Created by MarioH on 05/08/2018. */
abstract class BaseActivity : AppCompatActivity(), Tracing {
	protected val honeyApp by lazy { application as HoneypotApplication }
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder().permitAll().build())
	}

	/** Initializes components of the screen (JAVA) and some data for the well behaviour  */
	protected abstract fun startComponents()

	/** Initializes an image of each service used  */
	protected abstract fun startServiceImages()

	/** Update and verify the services are running  */
	protected abstract fun updateServices()

	/**
	 * Start the port's service
	 *
	 * @param debug True for debugging
	 * @param showToast True to appear Toasts
	 * @param image Settings for the service
	 */
	protected fun start(debug: Boolean = true, showToast: Boolean, image: ServiceImage) {
		HoneypotManager.msg = TRACER(HoneypotApplication.H_TAG, applicationContext, showToast)
		HoneypotManager.showToast = showToast

		if (!HoneypotManager.checkPort(image.portAdm)) // check if is already running
			HoneypotManager.startService(image, applicationContext, debug)
	}

	/**
	 * Stop the service
	 *
	 * @param debug True for debugging
	 * @param showToast True to appear Toasts
	 * @param image Settings for the service
	 */
	protected fun stop(debug: Boolean = true, showToast: Boolean, image: ServiceImage) {
		HoneypotManager.msg = TRACER(HoneypotApplication.H_TAG, applicationContext, showToast)

		if (HoneypotManager.checkPort(image.portAdm))
			HoneypotManager.requestStop(image.classService, applicationContext, debug)
	}
}