package br.mario.labsac.honeypotframework.serviceBasis

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service

import android.content.Intent

import android.os.Build
import android.os.IBinder

import android.support.annotation.CallSuper

import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat

import br.mario.labsac.honeypotframework.R
import br.mario.labsac.honeypotframework.app
import br.mario.labsac.honeypotframework.controller.HoneypotManager.EXTRA_SERVICE
import br.mario.labsac.honeypotframework.controller.HoneypotManager.EXTRA_DEBUG
import br.mario.labsac.honeypotframework.controller.HoneypotManager.KEY_IMAGE
import br.mario.labsac.honeypotframework.getBitmap
import br.mario.labsac.honeypotframework.protocol.HoneypotProtocol
import br.mario.labsac.honeypotframework.tools.RequestStopReceiver
import br.mario.labsac.honeypotframework.tools.TRACER
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.ERROR
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.INFO

import java.io.IOException

import java.lang.Exception
import java.lang.System.currentTimeMillis

import java.net.ServerSocket

private const val CHANNEL_ID = "services_running"
private const val ACTION = "LAUNCH_NOTIFICATION"

/**
 * This class is an Android Service which receives socket connections.
 *
 * @author Mário Henrique
 * Created on 09/03/2018.
 */
abstract class GeneralService(private val protocol: HoneypotProtocol) : Service() {
	//	/** verbose True - generate info messages in log  */
	//	private var verbose = true
	lateinit var msg: TRACER

	/** Obligatory. Give an integer Id to the notification if else throw an Exception */
	protected var notificationId: Int = 0
	/** Obligatory. Give an Icon resource id to the notification if else throw an Exception */
	protected var notificationIcon: Int = 0
	/** Obligatory. Give an Name to the notification if else throw an Exception */
	protected var notificationTitle: String? = null
	/** Optional. Give an Ticker string to the notification if else throw an Exception */
	protected var notificationTicker: String? = null

	private var port: Int = 0
	private var portAdm: Int = 0
	private var serverSocket: ServerSocket? = null
	/** debug True - generate debug info in log  */
	private var debug = true

	private lateinit var serviceName: String
	private lateinit var serverAdmSocket: ServerSocket
	private lateinit var mainProcess: ProcessRequests
	private lateinit var adminProcess: ProcessAdminRequests
	private lateinit var pendingIntent: PendingIntent

	private val notification by lazy {
		NotificationCompat.Builder(applicationContext, CHANNEL_ID)
	}

	init { createNotificationChannel() }

	final override fun onCreate() {
		msg = TRACER("SERVICE",this)

		protocol.setLogger(msg)
	}

	/**
	 * Important function to build the service. Here are some things to follow:
	 *
	 * 1. notificationId, notificationTitle, notificationIcon. If don't, the code will throw Exceptions
	 * 2. optional set an Ticker for the notification
	 * 3. call this super function last
	 */
	@CallSuper
	override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
		app = application

		notification.setShowWhen(true)
			.setVibrate(longArrayOf(500, 1000, 500))
			.setTicker(notificationTicker ?: getString(R.string.HPS_notification_ticker_default))
			.setSmallIcon(if (notificationIcon != 0) notificationIcon else throw Throwable(getString(R.string.HPS_notification_without_icon)))
			.setLargeIcon(getBitmap(notificationId))
			.setContentTitle(notificationTitle ?: throw Throwable(getString(R.string.HPS_notification_without_title)))

		pendingIntent = PendingIntent.getActivity(this, 0, Intent(ACTION),
			PendingIntent.FLAG_UPDATE_CURRENT)

		if (!parseExtras(intent))
			return START_NOT_STICKY // não válido

		// port = 23 // teste

		protocol.setPort(port)
		protocol.setName(serviceName)

		this.msg.log(INFO, "${msg.prop(R.string.HP_startingService, 1)}$serviceName")

		try {
			serverSocket = ServerSocket(port)
			this.msg.log(INFO, msg.prop(R.string.HP_showClientPort, 0) + port)

			try {
				serverAdmSocket = ServerSocket(portAdm)
				msg.log(INFO, msg.prop(R.string.HP_showAdminPort, 0) + portAdm)

				this.msg.log(INFO, msg.prop(R.string.HP_initClientThread, 0))
				mainProcess = ProcessRequests(serverSocket, protocol, msg)

				msg.log(INFO, msg.prop(R.string.HP_initAdminThread, 0))
				adminProcess = ProcessAdminRequests(serverAdmSocket, msg)

				// It's good when everything runs ok...
				this.msg.log(INFO, msg.prop(R.string.HP_onStartOk, 2))

				this.notification() // configure the notification

				return (START_REDELIVER_INTENT)

			} catch (e: IOException) {
				this.msg.log(ERROR, msg.prop(R.string.HP_errorOpeningAdminPort, 3))
			}

		} catch (e: Exception) {
			this.msg.log(ERROR, msg.prop(R.string.HP_errorOpeningPort, 4), e)
			// stopEverything()
		}

		return (START_NOT_STICKY)
	}

	final override fun onDestroy() {
		stopEverything()
		super.onDestroy()
	}

	/**
	 * Parse the extras from the Intent received
	 *
	 * @param intent The Intent what contains extras
	 * @return True if the all extras are ok
	 */
	private fun parseExtras(intent: Intent): Boolean {
		var result = true

		val extras = intent.extras
		val image = extras.get(KEY_IMAGE) as ServiceImage

		try {
			this.port = image.port
			this.msg.log(INFO, "porta: " + this.port)

			this.portAdm = image.portAdm
			this.msg.log(INFO, "porta admininstradora: " + this.portAdm)

		} catch (nfe: NumberFormatException) {
			this.msg.log(ERROR, msg.prop(R.string.HP_nonNumericPorts, 5))
			result = false
		}

		this.serviceName = image.nameService!!
		this.msg.log(INFO, "nameService: " + this.serviceName)

		this.debug = extras.getBoolean(EXTRA_DEBUG)
		this.msg.log(INFO, "debug: " + this.debug)

		//this.verbose = extras.getBoolean("verbose")
		//this.msg.log(DEBUG, "verbose: " + this.verbose)

		return (result)
	}

	private fun stopEverything() {
		this.msg.log(LOGTYPE.INFO, msg.prop(R.string.HP_stoppingService, 12) + ": " + serviceName)

		try {
			serverSocket!!.close()
			msg.log(LOGTYPE.DEBUG, msg.prop(R.string.HP_serverSocketIsClosed, 21))
			mainProcess.stop()
			msg.log(LOGTYPE.DEBUG, msg.prop(R.string.HP_mainThreadInterrupted, 30))

			serverAdmSocket.close()
			msg.log(LOGTYPE.DEBUG, msg.prop(R.string.HP_serverAdmSocketIsClosed, 22))
			adminProcess.stop()
			msg.log(LOGTYPE.DEBUG, msg.prop(R.string.HP_admThreadInterrupted, 31))

			msg.log(LOGTYPE.DEBUG, msg.prop(R.string.HP_cancelNotification, 0))
			NotificationManagerCompat.from(applicationContext).cancel(notificationId)

		} catch (e: IOException) {
			this.msg.log(LOGTYPE.ERROR, "eRRo: fechamento errado\n" + e.message)
			return
		}

		this.msg.log(LOGTYPE.DEBUG, String.format(msg.prop(R.string.HP_terminatingService, 0), port))
	}

	/** Função para realizar a notificação  */
	private fun notification() {
		msg.log(INFO, "Notifying!")

		// This is the intent of PendingIntent
		val intentAction = Intent(applicationContext, RequestStopReceiver::class.java)
		intentAction.putExtra(EXTRA_SERVICE, javaClass.name)

		val requestStop = PendingIntent.getBroadcast(applicationContext, 1, intentAction, 0) // PendingIntent.FLAG_UPDATE_CURRENT

		notification.setWhen(currentTimeMillis())
			.setContentText(getString(R.string.HPS_notification_ticker_default))
			.setContentIntent(pendingIntent)
			.addAction(android.R.drawable.ic_media_pause, getString(R.string.HPS_notification_stop_service), requestStop)
			.setOngoing(true)

		if (notificationId == 0) throw Throwable(getString(R.string.HPS_notification_without_id))
		NotificationManagerCompat.from(this).notify(notificationId, notification.build())
	}

	/** Create the NotificationChannel, but only on API 26+ because
	 * the NotificationChannel class is new and not in the support library */
	private fun createNotificationChannel() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			val name = getString(R.string.channel_name)
			val description = getString(R.string.channel_description)
			val importance = NotificationManager.IMPORTANCE_DEFAULT
			val channel = NotificationChannel(CHANNEL_ID, name, importance)
			channel.description = description

			getSystemService(NotificationManager::class.java).createNotificationChannel(channel)
		}
	}

	final override fun onBind(arg0: Intent): IBinder? = (null)
}