package br.mario.labsac.honeypotframework.protocol

import java.io.DataOutputStream
import java.io.IOException

/**
 * Supports the creation of a protocol
 *
 * @author Mário Henrique
 * Created on 10/03/2018.
 */
abstract class ProtocolResponse(private val output: DataOutputStream) {
	/**
	 * Method called to respond to client requests. Calls the corresponding method in accordance with
	 * its implementation in the child class.
	 *
	 * @param line Message received from the client
	 * @return **True** if there is a disconnection request from the client. **False** if not
	 * @throws IOException any exception of I/O is treated outside
	 */
	@Throws(IOException::class)
	abstract fun respond(line: String): Boolean

	/**
	 * Method to send the response to the client
	 *
	 * @param answer The answer to be sent
	 * @throws IOException any exception of I/O is treated outside
	 */
	@Throws(IOException::class)
	protected fun flush(answer: String) {
		output.write(answer.toByteArray())
		output.flush()
	}
}