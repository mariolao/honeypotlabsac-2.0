package br.mario.labsac.honeypotframework.tools

import android.util.Log
import br.mario.labsac.honeypotframework.appHoney
import org.jetbrains.anko.toast

/** Created by MarioH on 11/08/2018. */
interface Tracing {
	/** Note that the tag length should not be more than 23 symbols. */
	val logTag: String
		get() = getTag(javaClass)
}

fun Tracing(clazz: Class<*>): Tracing = object : Tracing {
	override val logTag = getTag(clazz)
}

fun Tracing(tag: String)  = object : Tracing {
	init { assert(tag.length <= 23) { "The maximum tag length is 23, got $tag" } }
	override val logTag = tag
}

inline fun <reified T: Any> Tracing(): Tracing = Tracing(T::class.java)

fun Tracing.verbose(message: Any, thr: Throwable? = null) {
	log(this, message, thr, Log.VERBOSE,
		{tag, msg -> Log.v(tag, msg)},
		{tag, msg, t -> Log.v(tag, msg, t)})
}

fun Tracing.debug(message: Any, thr: Throwable? = null) {
	log(this, message, thr, Log.DEBUG,
		{ tag, msg -> Log.d(tag, msg) },
		{ tag, msg, t -> Log.d(tag, msg, t) })
}

fun Tracing.info(message: Any, thr: Throwable? = null) {
	log(this, message, thr, Log.INFO,
		{ tag, msg -> Log.d(tag, msg) },
		{ tag, msg, t -> Log.d(tag, msg, t) })
}

fun Tracing.warn(message: Any, thr: Throwable? = null) {
	log(this, message, thr, Log.WARN,
		{ tag, msg -> Log.d(tag, msg) },
		{ tag, msg, t -> Log.d(tag, msg, t) })
}

fun Tracing.error(message: Any, line: Int = 0, thr: Throwable? = null, show: Boolean = false) {
	val mes = if (line != 0) "$message [l: $line]" else "$message"
	//	val mes = "$message" + (line ?: "")
	log(this, mes, thr, Log.ERROR,
		{ tag, msg -> Log.d(tag, msg) },
		{ tag, msg, t -> Log.d(tag, msg, t) })
	if (show)
		appHoney.toast(mes)
}

fun Tracing.error(idRes: Int, line: Int = 0, thr: Throwable? = null, show: Boolean = false) {
	val mes = if (line != 0) "${appHoney.prop(idRes)} [l: $line]" else appHoney.prop(idRes)
	log(this, mes, thr, Log.ERROR,
		{ tag, msg -> Log.d(tag, msg) },
		{ tag, msg, t -> Log.d(tag, msg, t) })
	if (show)
		appHoney.toast(mes)
}

fun Tracing.wtf(message: Any?, thr: Throwable? = null) {
	if (thr != null)
		Log.wtf(logTag, message?.toString() ?: "null", thr)
	else
		Log.wtf(logTag, message?.toString() ?: "null")
}

private inline fun log(
	logger: Tracing,
	message: Any,
	thr: Throwable?,
	level: Int,
	f: (String, String) -> Unit,
	fThrowable: (String, String, Throwable) -> Unit) {
	val tag = logger.logTag
	if (Log.isLoggable(tag, level)) {
		if (thr != null)
			fThrowable(tag, message.toString(), thr)
		else
			f(tag, message.toString())
	}
}

private fun getTag(clazz: Class<*>): String {
	val tag = clazz.simpleName
	return if (tag.length <= 23) {
		tag
	} else {
		tag.substring(0, 23)
	}
}