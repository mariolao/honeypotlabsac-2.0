package br.mario.labsac.honeypotframework.controller

import android.app.AlarmManager
import android.app.PendingIntent

import android.content.Context
import android.content.Intent
import android.content.res.Resources

import android.os.Bundle
import android.os.SystemClock

import br.mario.labsac.honeypotframework.R
import br.mario.labsac.honeypotframework.getInt
import br.mario.labsac.honeypotframework.tools.TransferLogToServer

import org.jetbrains.anko.toast

/**
 * Base class to build a Activity without an UI.
 * It starts the all the services as soon as the application opens
 *
 * @author Mário Henrique
 * Created on 29/07/2018.
 * source: https://stackoverflow.com/a/10909723/3443949
 */
abstract class ActivityTranslucent : BaseActivity() {
	protected open var timeInMilliSeconds: Long = 0
	//	protected var listOfImages = Vector<ServiceImage>()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		this.startComponents()

		this.scheduleAlarm()

		this.updateServices()
		this.startServiceImages()
		this.startImages()

		finish()
	}

	/** Implement a start function for each service image */
	protected abstract fun startImages()

	protected fun toaster(vararg args: String) {
		args.forEach { toast("Service started: $it") }
	}

	private fun scheduleAlarm() {
		val it = Intent(application, TransferLogToServer::class.java)

		val alarm = getSystemService(Context.ALARM_SERVICE) as AlarmManager
		val triggerTime = SystemClock.elapsedRealtime() + (20 * 1000) //cal.timeInMillis

		if (this.timeInMilliSeconds == 0L) this.timeInMilliSeconds = getInt(R.integer.timeAlarm).toLong()

		alarm.setInexactRepeating(
			AlarmManager.ELAPSED_REALTIME,
			triggerTime,
			timeInMilliSeconds,
			PendingIntent.getService(this, 0, it, 0)
		)
	}

	final override fun getTheme(): Resources.Theme =
		super.getTheme().apply { applyStyle(android.R.style.Theme_Translucent_NoTitleBar, true) }
}