package br.mario.labsac.honeypotframework.serviceBasis

import br.mario.labsac.honeypotframework.R
import br.mario.labsac.honeypotframework.tools.TRACER
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.DEBUG
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.ERROR
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.INFO

import java.io.IOException
import java.net.ServerSocket

/**
 * Class to verify if a specific service is running
 *
 * @author Mário Henrique
 * Created on 17/03/2018.
 */
class ProcessAdminRequests(private val srvSock: ServerSocket, private val msg: TRACER) {
	private var thread: Thread = Thread {
		while (true) {
			try {
				msg.log(DEBUG, msg.prop(R.string.HP_waitingAdminConnection, 0))
				srvSock.accept()

				Thread.sleep(100)

			} catch (ioe: IOException) {
				msg.log(INFO, msg.prop(R.string.HP_exceptionAdminRequest, 15))
				break
			} catch (e: InterruptedException) {
				msg.log(ERROR, msg.prop(R.string.HP_exceptionAdminSocket, 0))
				break
			}
		}
	}

	init { thread.start() }

	fun stop() = thread.interrupt()
}