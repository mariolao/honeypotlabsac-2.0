package br.mario.labsac.honeypotframework.controller

import android.annotation.SuppressLint

import android.app.Application

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import android.content.res.Resources.NotFoundException

import br.mario.labsac.honeypotframework.R
import br.mario.labsac.honeypotframework.appHoney
import br.mario.labsac.honeypotframework.getInt
import br.mario.labsac.honeypotframework.getLong

private const val MY_PREFS = "MY_PREFS"

/**
 * Class that encapsulates stuff related to the entire application
 *
 * @author Mário Henrique
 * Created on 02/03/2018.
 */
class HoneypotApplication : Application() {
	val preferences: SharedPreferences by lazy { getSharedPreferences(MY_PREFS, Context.MODE_PRIVATE) }
	val mapOfThings = mutableMapOf<String, Any>()

	val editor: Editor
		@SuppressLint("CommitPrefEdits")
		get() = preferences.edit()

	override fun onCreate() {
		super.onCreate()

		H_TAG = getString(R.string.TAG)
		// preferences = getSharedPreferences(MY_PREFS, Context.MODE_PRIVATE)
		appHoney = this
	}

	fun getIntPreference(namePref: String, id_defValue: Int) = (preferences.getInt(namePref, getInt(id_defValue)))

	fun getLongPreference(namePref: String, id_defValue: Int) = (preferences.getLong(namePref, getLong(id_defValue)))

	fun getStringPreference(namePref: String,
	                        id_defValue: Int): String = preferences.getString(namePref, getString(id_defValue))

	fun prop(resourceId: Int, line: Int = 0, nClass: String = ""): String {
		return (
			try {
				"${this.getString(resourceId)} [$nClass l: $line]"

			} catch (rnf: NotFoundException) {
				("HP message not found")
			}
		)
	}

	companion object {
		lateinit var H_TAG: String
	}
}