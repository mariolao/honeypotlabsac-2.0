package br.mario.labsac.honeypotframework.tools

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

import br.mario.labsac.honeypotframework.R
import br.mario.labsac.honeypotframework.controller.HoneypotManager.EXTRA_SERVICE
import br.mario.labsac.honeypotframework.controller.HoneypotManager.requestStop
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.INFO

/**
 * Broadcast that receives the shutdown action from the stop notification button of a service
 *
 * @author Mário Henrique
 * Created on 29/07/2018.
 * source: https://stackoverflow.com/a/41313003/3443949
 */
internal class RequestStopReceiver : BroadcastReceiver() {
	override fun onReceive(ctx: Context, it: Intent) {
		val msg = TRACER("STOP_RECEIVER", ctx, true)

		val className = it.getStringExtra(EXTRA_SERVICE)
		msg.log(INFO, msg.prop(R.string.HPB_requestShutDown, 0) + className)

		requestStop(Class.forName(className), ctx, true)

		//This is used to close the notification tray
		// ctx.sendBroadcast(Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS))
	}
}