package br.mario.labsac.honeypotframework.protocol

import br.mario.labsac.honeypotframework.R
import br.mario.labsac.honeypotframework.getFileStr
import br.mario.labsac.honeypotframework.removeSpace
import br.mario.labsac.honeypotframework.getStrDate
import br.mario.labsac.honeypotframework.tools.TRACER
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.ERROR
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.INFO
import br.mario.labsac.honeypotframework.tools.LogSaver

import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking

import java.io.BufferedReader
import java.io.DataOutputStream
import java.io.IOException
import java.io.InputStreamReader

import java.net.Inet4Address
import java.net.NetworkInterface
import java.net.Socket
import java.net.SocketException

import java.util.Calendar

/**
 * Class to encapsulate Protocol data of a Service
 *
 * @author Mário Henrique
 * Created on 09/03/2018.
 */
class HoneypotProtocol(private val getResponse: (DataOutputStream) -> ProtocolResponse ) {
	private var destinationIp: String? = null
		get() =  if (field == null) {field = _destinationIp; _destinationIp} else field
	private var destinationPort: Int = 0
	private var sourceIp: String? = null
	private var sourcePort: Int = 0

	private lateinit var nameService: String
	private lateinit var msg: TRACER

	/**
	 * To know IP of the device
	 *
	 * @return Device's IP
	 */
	private val _destinationIp: String?
		get() {
			try {
				val en = NetworkInterface.getNetworkInterfaces()
				while (en.hasMoreElements()) {
					val intf = en.nextElement()

					val enumIpAddr = intf.inetAddresses
					while (enumIpAddr.hasMoreElements()) {
						val inetAddress = enumIpAddr.nextElement()
						if ((inetAddress is Inet4Address) and (!inetAddress.isLoopbackAddress))
							return inetAddress.hostAddress.toString()
					}
				}

			} catch (ex: SocketException) {
				msg.log(ERROR, ex.message!!)
			}

			return (null)
		}

	/** Process a request. Socket "s" was accepted by the service. Receive the socket  */
	internal fun processRequest(s: Socket) {
		val archStamp = Calendar.getInstance().getFileStr()
		runBlocking {
			launch(context) {
				this@HoneypotProtocol.sourceIp = s.inetAddress.hostAddress
				this@HoneypotProtocol.sourcePort = s.port

				lateinit var br: BufferedReader
				lateinit var dos: DataOutputStream
				lateinit var response: ProtocolResponse

				try {
					msg.log(INFO, msg.prop(R.string.HP_processing, 105)
						+ " Service: $nameService - Socket: $s")

					br = BufferedReader(InputStreamReader(s.getInputStream()))
					dos = DataOutputStream(s.getOutputStream()) // ExecutorResponse.getOutputStream(s);

					response = getResponse.invoke(dos)

				} catch (exp: IOException) {
					msg.log(ERROR, msg.prop(R.string.HP_ioExceptionProcessingProtocol, 102)
						+ " ## THREAD: "
						+ Thread.currentThread().id)
					return@launch
				}

				while (true) {
					try {
						if (s.isClosed) break

						var linea: String? = br.readLine()
						val string = " $sourceIp = $linea"
						msg.log(INFO, msg.prop(R.string.HP_commandReceived, 101) + " " + string)

						while (linea == null)
							linea = br.readLine()

						if (response.respond(linea)) {
							saveRequest(linea, archStamp)
							break // if true;
						}

						saveRequest(linea, archStamp)

					} catch (ioe: IOException) {
						msg.log(ERROR, msg.prop(R.string.HP_ioExceptionProcessingCommand, 102) + " ## THREAD: "
							+ Thread.currentThread().name)
						break
					}
				}

				try {
					dos.close()

				} catch (e: NullPointerException) {
					msg.log(ERROR, e.message!!, e)
				} catch (exp: IOException) { exp.printStackTrace() }
			} // end of launch
		}
	}

	/**
	 * Saves the interaction of the attacker and this honeypot.
	 * Creates an instance of TrailSaver to process in a different thread the saving on the log's archive
	 *
	 * @param trail The content of the commands written
	 */
	private fun saveRequest(trail: String, hashCode: String) {
		val dateTime = Calendar.getInstance().getStrDate('\\')
		val logLine = "$dateTime tcp $sourceIp $sourcePort $destinationIp $destinationPort $trail\n"

		val archiveName = ("${nameService}_$hashCode").toLowerCase().removeSpace()
		LogSaver(archiveName, logLine)
	}

	internal fun setPort(port: Int) {
		this.destinationPort = port
	}

	/**
	 * Set a name to the service.
	 * This name will be use in the creation of notification, log's archive and debug output
	 */
	internal fun setName(serverName: String) {
		this.nameService = serverName
	}

	internal fun setLogger(msg: TRACER) {
		this.msg = msg
	}
}