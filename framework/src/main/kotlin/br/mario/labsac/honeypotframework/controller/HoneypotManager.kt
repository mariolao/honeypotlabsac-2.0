package br.mario.labsac.honeypotframework.controller

import android.annotation.SuppressLint

import android.content.Context
import android.content.Intent

import android.net.ConnectivityManager

import br.mario.labsac.honeypotframework.R
import br.mario.labsac.honeypotframework.appHoney
import br.mario.labsac.honeypotframework.serviceBasis.ServiceImage
import br.mario.labsac.honeypotframework.tools.TRACER
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.INFO
import br.mario.labsac.honeypotframework.tools.Tracing
import br.mario.labsac.honeypotframework.tools.error

import java.io.IOException

import java.net.InetAddress
import java.net.NetworkInterface
import java.net.Socket
import java.net.SocketException
import java.net.UnknownHostException

/**
 * This object is resposible to manage the status of the services
 *
 * @author Mário Henrique
 * Created on 06/03/2018.
 */
object HoneypotManager : Tracing {
	//		var debug: Boolean = false // var verbose: Boolean = true
	@SuppressLint("StaticFieldLeak")
	internal var msg: TRACER? = null

	@JvmSynthetic
	internal const val EXTRA_SERVICE = "class"
	@JvmSynthetic
	internal const val EXTRA_DEBUG = "debug"
	@JvmSynthetic internal const val KEY_IMAGE = "image"

	override val logTag = "Manager"

	var showToast: Boolean = false

	/**
	 * Check network connectivity.
	 *
	 * @return A list of IP Addresses available or an empty list.
	 */
	@JvmStatic
	fun checkNetwork(context: Context): List<String> {
		val addresses = ArrayList<String>()
		val conMgr = context
			.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
		val info = conMgr.activeNetworkInfo
		var ipa = ""

		if (info != null) {
			if (info.isAvailable) {
				if (info.isConnected) {
					try {
						val nis = NetworkInterface
							.getNetworkInterfaces()
						while (nis.hasMoreElements()) {
							val ni = nis.nextElement()
							val ipads = ni.inetAddresses
							while (ipads.hasMoreElements()) {
								val ipad = ipads.nextElement()
								ipa = ""
								if (!ipad.isLoopbackAddress) {
									ipa += ipad.hostAddress.toString()
									if (ipad.isSiteLocalAddress) {
										ipa += " (LOCAL)"
									}
								}
							}
							if (ipa.isNotEmpty()) addresses.add(ipa)
						}

					} catch (ex: SocketException) {
						// msg?.log(ERROR, msg?.prop(R.string.HPS_socketException, 50) + " " + ex.message)
						error(R.string.HPS_socketException, 85, ex, showToast)
					}

				} else {
					// msg?.log(ERROR, msg!!.prop(R.string.HPS_actNotConnected, 51))
					error(appHoney.prop(R.string.HPS_actNotConnected), 90, show = showToast)
				}
			} else {
				// msg?.log(ERROR, msg!!.prop(R.string.HPS_actConNotAvailable, 52))
				error(R.string.HPS_actConNotAvailable, 94, show = showToast)
			}
		} else {
			// msg?.log(ERROR, msg!!.prop(R.string.HPS_actConNotAvailableNull, 53))
			error(R.string.HPS_actConNotAvailableNull, 98, show = showToast)
		}

		return (addresses)
	}

	/**
	 * Verify if the service is running by making a request to the admin port.
	 *
	 * @param port
	 * Port number
	 * @return true - The service is running. False - the service is not running.
	 */
	@JvmStatic
	fun checkPort(port: Int): Boolean {
		var resultado = false

		val local: InetAddress
		var s: Socket? = null

		try {
			local = InetAddress.getByName("localhost") // 10.0.2.2
			s = Socket(local, port)

			resultado = true

		} catch (e: UnknownHostException) {
			// msg?.log(ERROR, "Host's name is not supported")
			error("Host's name is not supported", 126, e, showToast)
		} catch (e: IOException) {
			// msg?.log(ERROR, "Socket connection is not started")
			error("Socket connection is not started", 129, e, showToast)
		} finally {
			try {
				s?.close()

			} catch (e: IOException) {
				// msg?.log(ERROR, "eRRor closing the socket!")
				error("eRRor closing the socket!", 136, e, showToast)
			}

		}

		return (resultado)
	}

	/**
	 * Start Honeypot Service.
	 *
	 * @param image Image of the Service
	 * @param context Android Context
	 */
	fun startService(image: ServiceImage, context: Context, debug: Boolean) {
		if (debug)
			msg?.log(INFO, "start the service: ${image.classService}")

		context.startService(Intent(context, image.classService).apply {
			putExtra(EXTRA_DEBUG, debug) // intent.putExtra("verbose", verbose)
			putExtra(KEY_IMAGE, image)
		})
	}

	/**
	 * Request to stop the service.
	 *
	 * @param classService Class to be stopped
	 */
	@JvmStatic
	fun requestStop(classService: Class<*>, context: Context, debug: Boolean) {
		if (debug)
			msg?.log(INFO,
				(msg?.prop(R.string.HP_terminateByRequest, 19)) + classService.toString())
		context.stopService(Intent(context, classService))
	}
}