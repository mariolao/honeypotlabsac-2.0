package br.mario.labsac.honeypotframework.serviceBasis

import java.io.Serializable

/**
 * Class that encapsulate all required data for a service execution
 *
 * @author Mário Henrique
 * Created on 06/03/2018.
 */
class ServiceImage(val port: Int = 0, val portAdm: Int, val nameService: String?, val classService: Class<out GeneralService>) :
	Serializable