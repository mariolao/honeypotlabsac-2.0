package br.mario.labsac.honeypotframework.serviceBasis

import br.mario.labsac.honeypotframework.R
import br.mario.labsac.honeypotframework.R.string
import br.mario.labsac.honeypotframework.protocol.HoneypotProtocol
import br.mario.labsac.honeypotframework.tools.TRACER
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.ERROR
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.INFO

import java.io.IOException
import java.lang.Exception
import java.net.ServerSocket

/** Created by MarioH on 20/07/2018. */
class ProcessRequests(private val srvSock: ServerSocket?, private val protocol: HoneypotProtocol, private val msg: TRACER) {
	private var thread = Thread {
		while (true) {
			try {
				if (Thread.interrupted()) throw InterruptedException()
				if (srvSock == null || srvSock.isClosed) throw InterruptedException()

				msg.log(INFO, msg.prop(R.string.HP_waitingClientConnection, 0))
				val s = srvSock.accept() // creates a new thread for each new client
				msg.log(INFO, "${msg.prop(R.string.HP_clientConnected, 0)}: ${s.inetAddress} | ${s.localAddress}")

				try {
					protocol.processRequest(s)
					msg.log(INFO, msg.prop(string.HP_clientIsClosing, 0))

					s.close()

				} catch (excp: Exception) {
					msg.log(ERROR, msg.prop(R.string.HP_errorClosingSocket, 31) + excp.message)
				} finally {
					msg.log(INFO, msg.prop(R.string.HP_clientDisconnected, 0))
				}

			} catch (ioe: IOException) {
				msg.log(INFO, msg.prop(R.string.HP_ioExceptionProcessingProtocol, 17))
				break
			} catch (e: InterruptedException) {
				msg.log(INFO, msg.prop(R.string.HP_serverSocketIsClosed, 21))
				break
			}

		}
	}

	init {
		msg.log(INFO, "executando após aceitar conexão")
		thread.start()
	}

	fun stop() = thread.interrupt()
}