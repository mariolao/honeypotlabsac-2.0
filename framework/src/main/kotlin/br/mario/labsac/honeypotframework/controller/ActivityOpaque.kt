package br.mario.labsac.honeypotframework.controller

import android.app.AlarmManager
import android.app.PendingIntent

import android.content.Context
import android.content.Intent

import android.os.Bundle
import android.os.Handler
import android.os.SystemClock

import android.support.annotation.CallSuper

import br.mario.labsac.honeypotframework.KEY_TIMEALARM
import br.mario.labsac.honeypotframework.R
import br.mario.labsac.honeypotframework.controller.HoneypotManager.requestStop
import br.mario.labsac.honeypotframework.tools.Tracing
import br.mario.labsac.honeypotframework.tools.TransferLogToServer

import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.info

/**
 * Base class to build a Activity and provide an UI for user
 *
 * @author Mário Henrique
 * Created on 09/03/2018.
 */
@Suppress("PropertyName", "MemberVisibilityCanBePrivate")
abstract class ActivityOpaque : BaseActivity(), AnkoLogger {
//	protected var ipServer: String? = null
//	protected var portServer: Int = 0
	protected var timeInMilliSeconds: Long = 0

	private lateinit var alarm: AlarmManager

	val log = Tracing("HoneypotActivity")
	val t = AnkoLogger<ActivityOpaque>()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		super.setContentView(R.layout.activity_initial)

//		this.ipServer = honeyApp.getStringPreference(KEY_IPSERVER, R.string.ipServer)
		this.startComponents()

		this.scheduleAlarm()
	}

	override fun onResume() {
		super.onResume()
		this.startServiceImages()
		this.updateLayout()
	}

	/**
	 * Update the Layout. Redraw the components called in updater function property
	 *
	 * @param time Time to wait and execute the procedure
	 */
	@CallSuper
	protected fun updateLayout(time: Long = 500) {
		val postDelayed = Handler().postDelayed({
			info { "<-- configuring layout" }
			updateServices()
			updater?.invoke()
			info { "layout configured -->" }
		}, time)
		if (!postDelayed) error("eRRoR: didn't sleep ")
	}

	protected abstract val updater: (() -> Unit)?

	private val pendingIntent by lazy { PendingIntent.getService(this, 0,
		Intent(application, TransferLogToServer::class.java), 0) }

	protected fun rebootAlarm() {
		alarm.cancel(pendingIntent)

		requestStop(TransferLogToServer::class.java, this, true)

		scheduleAlarm()
	}

	private fun scheduleAlarm() {
		alarm = getSystemService(Context.ALARM_SERVICE) as AlarmManager
		val triggerTime = SystemClock.elapsedRealtime() + (20 * 1000)//cal.timeInMillis

		if (this.timeInMilliSeconds == 0L)
			this.timeInMilliSeconds = honeyApp.getLongPreference(KEY_TIMEALARM, R.integer.timeAlarm)

		alarm.setInexactRepeating(
			AlarmManager.ELAPSED_REALTIME,
			triggerTime,
			timeInMilliSeconds,
			pendingIntent
		)
	}
}