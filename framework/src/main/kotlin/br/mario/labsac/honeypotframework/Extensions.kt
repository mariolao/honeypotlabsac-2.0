@file:JvmName("Extensions")
package br.mario.labsac.honeypotframework

import android.annotation.TargetApi

import android.app.Application

import android.content.Context

import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable

import android.os.Build.VERSION
import android.os.Build.VERSION_CODES

import android.support.annotation.DrawableRes

import br.mario.labsac.honeypotframework.controller.HoneypotApplication

import java.io.Closeable
import java.text.SimpleDateFormat

import java.util.Calendar
import java.util.Locale

/**
 * File that contains some constants and auxiliar properties and functions to the project
 *
 * @author Mário Henrique
 * Created on 15/03/2018.
 */
const val KEY_IPSERVER = "ipServer"
const val KEY_PORTSERVER = "portServer"
/** it refers to the time of alarm that triggers the action of sending logs to the server. It's value must be Long */
const val KEY_TIMEALARM = "alarm"

var app: Application = Application() // todo testar se não há falhas
var appHoney: HoneypotApplication = HoneypotApplication()

fun Context.getInt(idRes: Int) = resources.getInteger(idRes)

fun Context.getLong(idRes: Int) = resources.getInteger(idRes).toLong()

fun Context.drawable(@DrawableRes idRes: Int): Drawable =
	if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
		resources.getDrawable(idRes, null)
	} else {
		@Suppress("DEPRECATION")
		resources.getDrawable(idRes)
	}

fun Context.getBitmap(@DrawableRes idRes: Int) = BitmapFactory.decodeResource(resources, idRes)

fun String.removeSpace(): String {
	return replace(" ", "")
}

fun Calendar.getStrDate(c: Char): String { // String.format(Locale.getDefault(), "%1\$tF-%1\$tT", this)
	return (SimpleDateFormat("dd${c}MM${c}yyyy hh:mm:ss", Locale.getDefault()).format(this.time))
}

fun Calendar.getFileStr(): String {
	val dateF = SimpleDateFormat("dd-MM-yyyy_hh-mm-ss", Locale.getDefault())
	return (dateF.format(this.time))
}

@TargetApi(VERSION_CODES.KITKAT)
inline fun <T : Closeable?> Array<T>.use(block: ()->Unit) {
	var exception: Throwable? = null
	try {
		return block()
	} catch (e: Throwable) {
		exception = e
		throw e
	} finally {
		when (exception) {
			null -> forEach { it?.close() }
			else -> forEach {
				try {
					it?.close()

				} catch (closeException: Throwable) {
					exception.addSuppressed(closeException)
				}
			}
		}
	}
}

//@RequireKotlin("1.2", versionKind = RequireKotlinVersionKind.COMPILER_VERSION, message = "Requires newer compiler version to be inlined correctly.")
inline fun <T : Closeable?, R> T.use(execute: (Exception) -> Unit, block: (T) -> R): R {
	var exception: Throwable? = null
	try {
		return block(this)

	} catch (e: Throwable) {
		exception = e
		execute.invoke(e as Exception)
		throw e
	} finally {
		when {
			this == null -> {}
			exception == null -> close()
			else ->
				try {
					close()

				} catch (closeException: Throwable) {

				}
		}
	}
}