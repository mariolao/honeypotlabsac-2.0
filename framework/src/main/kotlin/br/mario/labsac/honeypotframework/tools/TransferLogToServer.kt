package br.mario.labsac.honeypotframework.tools

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service

import android.content.Intent
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat

import br.mario.labsac.honeypotframework.KEY_IPSERVER
import br.mario.labsac.honeypotframework.KEY_PORTSERVER

import br.mario.labsac.honeypotframework.R
import br.mario.labsac.honeypotframework.controller.HoneypotApplication
import br.mario.labsac.honeypotframework.getInt
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.ERROR
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.INFO

import java.io.File
import java.io.FileInputStream
import java.io.ObjectOutputStream

import java.net.InetAddress
import java.net.Socket

private const val CHANNEL_ID = "error_transfer"

/**
 * Class to send the log's archives to the Server
 *
 * @author Mário Henrique
 * Created on 06/03/2018.
 */
class TransferLogToServer : Service() {
	private lateinit var msg: TRACER

	override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
		msg = TRACER("SENDER", applicationContext, false)
		msg.log(INFO, "file transfer service")

		var notify = 4040

		loadArchives()?.forEach { arch ->
			msg.log(INFO, "archive: ${arch.name}")
			TransferArchiveExecutor(arch, ++notify)
		}

		return (super.onStartCommand(intent, flags, startId))
	}

	private fun loadArchives() = (File(this.filesDir, getString(R.string.log_folder)).listFiles())

	/**
	 * Class responsible to build the Thread that sends the archive
	 *
	 * @author Mário Henrique
	 */
	internal inner class TransferArchiveExecutor(private val theArchive: File, private val id: Int) {
		init {
			Thread {
				val sd = (application as HoneypotApplication).preferences

				val ipServer = sd.getString(KEY_IPSERVER, getString(R.string.ipServer))
				val portServer = sd.getInt(KEY_PORTSERVER, getInt(R.integer.portServer))

				msg.log(INFO, "IP: ${ipServer!!}") // DEBUG
				msg.log(INFO, "Port: $portServer")

				try {
					val socket = Socket(InetAddress.getByName(ipServer), portServer)
					val out = ObjectOutputStream(socket.getOutputStream())

					msg.log(INFO, "${msg.prop(R.string.HPA_transfering, 0)} ${theArchive.name}")

					out.writeUTF(theArchive.name)
					out.writeLong(theArchive.length())

					val `in` = FileInputStream(theArchive) // openFileInput(theArchive.name)
					val buf = ByteArray(4096)

					while (true) {
						val len = `in`.read(buf)
						if (len == -1)
							break
						out.write(buf, 0, len)
					}
					out.close()
					socket.close()

					msg.log(INFO, msg.prop(R.string.HPA_transfer_ok, 0))

				} catch (e: Exception) {
					msg.log(ERROR, msg.prop(R.string.HPA_transfer_error, 0), e)
					errorNotification(theArchive.name, e.message!!)
				}
			}.start()
		}

		private fun errorNotification(name: String, error: String) {
			//			intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
			val p = PendingIntent.getActivity(this@TransferLogToServer, 0,
				Intent("LAUNCH_NOTIFICATION"), PendingIntent.FLAG_UPDATE_CURRENT)

			/**
			 * 1- icon appears in device notification bar and right hand corner of notification
			 * 2- Content title, which appears in large type at the top of the notification
			 * 3- Content text, which appears in smaller text below the title
			 * 4- The subtext, which appears under the text on newer devices.
			 * 5- This will show-up in the devices with Android 4.2 and above only
			 * 6- Set the intent that will fire when the user taps the notification.
			 */
			val builder = NotificationCompat.Builder(application, CHANNEL_ID)
				.apply {
					setSmallIcon(android.R.drawable.stat_notify_error)
					setContentTitle(msg.prop(R.string.HPA_transfer_error, 0))
					setContentText(msg.prop(R.string.HPA_notification_error_1, 0) + " " + name)
					setSubText(msg.prop(R.string.HPA_notification_error_2, 0) + " " + error)
					setStyle(NotificationCompat.BigTextStyle()
						.bigText(msg.prop(R.string.HPA_notification_error_1, 0) + " " + name))
					setContentIntent(p)
					setTimeoutAfter(20000)
					setAutoCancel(true) // setWhen(currentTimeMillis())
					setVibrate(longArrayOf(300, 500, 300))
				}

			NotificationManagerCompat.from(this@TransferLogToServer).notify(id, builder.build())
		}
	}

	/** Create the NotificationChannel, but only on API 26+ because
	 * the NotificationChannel class is new and not in the support library */
	private fun createNotificationChannel() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			val name = getString(R.string.channel_name)
			val description = getString(R.string.channel_description)
			val importance = NotificationManager.IMPORTANCE_DEFAULT
			val channel = NotificationChannel(CHANNEL_ID, name, importance)
			channel.description = description

			getSystemService(NotificationManager::class.java).createNotificationChannel(channel)
		}
	}

	override fun onBind(intent: Intent) = null
}