package br.com.mario.honeypotlabsac.protocol.telnet

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent

import android.content.BroadcastReceiver
import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_BATTERY_CHANGED
import android.content.IntentFilter

import android.database.CursorIndexOutOfBoundsException

import android.graphics.Color

import android.media.RingtoneManager

import android.os.BatteryManager
import android.os.Build
import android.os.Environment

import android.provider.CallLog.Calls

import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat

import br.com.mario.honeypotlabsac.R
import br.com.mario.honeypotlabsac.protocol.telnet.Sensors.accel
import br.com.mario.honeypotlabsac.protocol.telnet.Sensors.distance
import br.com.mario.honeypotlabsac.protocol.telnet.Sensors.gyro
import br.com.mario.honeypotlabsac.protocol.telnet.Sensors.gyroStatus
import br.com.mario.honeypotlabsac.protocol.telnet.Sensors.humi
import br.com.mario.honeypotlabsac.protocol.telnet.Sensors.lux
import br.com.mario.honeypotlabsac.protocol.telnet.Sensors.magn
import br.com.mario.honeypotlabsac.protocol.telnet.Sensors.orient
import br.com.mario.honeypotlabsac.protocol.telnet.Sensors.pressure
import br.com.mario.honeypotlabsac.protocol.telnet.Sensors.proxStatus
import br.com.mario.honeypotlabsac.protocol.telnet.Sensors.startListeners
import br.com.mario.honeypotlabsac.protocol.telnet.Sensors.stopListeners
import br.com.mario.honeypotlabsac.protocol.telnet.Sensors.temp
import br.com.mario.honeypotlabsac.protocol.telnet.util.Connectivity
import br.com.mario.honeypotlabsac.protocol.telnet.util.Telephony
import br.com.mario.honeypotlabsac.protocol.telnet.util.Telephony.Sms
import br.com.mario.honeypotlabsac.protocol.telnet.util.Telephony.Sms.Inbox
import br.com.mario.honeypotlabsac.protocol.telnet.util.Telephony.TextBasedSmsColumns

import br.mario.labsac.honeypotframework.app
import br.mario.labsac.honeypotframework.protocol.ProtocolResponse
import br.mario.labsac.honeypotframework.tools.TRACER
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.ERROR
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.INFO
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.WARN

import me.everything.providers.android.calllog.Call
import me.everything.providers.android.calllog.CallsProvider
import me.everything.providers.android.telephony.TelephonyProvider

import java.io.DataOutputStream
import java.util.regex.Pattern

// CONSTANTS:
private const val OK = "OK\r\n"
private const val RN = "\r\n"
private const val SUB_COMMANDS = "available sub-commands:\r\n"
private const val MISS_SUB_COMMANDS = "KO: missing sub-command\r\n"
private const val BAD_SUB_COMMAND = "KO: bad sub-command\r\n"

private const val CHANNEL_ID = "send_sms"

@Suppress("LocalVariableName")
/**
 * Classe responsável por gerenciar as respostas correspondentes às requisições que chegam do atacante
 *
 * @author Mário Henrique
 * Created on 14/03/2018.
 */
class TelnetResponse
constructor(dos: DataOutputStream) : ProtocolResponse(dos) {
	private val state = arrayOfNulls<String>(6)
	private val health = arrayOfNulls<String>(7)
	private val plugged = arrayOfNulls<String>(3)

	private val sendIntent: Intent
	private val notification by lazy {
		createNotificationChannel()
		NotificationCompat.Builder(app, CHANNEL_ID)
	}
	private val notificationManager by lazy { NotificationManagerCompat.from(app) }
	private val logger by lazy { TRACER("RESPONSE", app) }

	// variables:
	private var present: Boolean = false
	private val notificationId: Int = 0
	private var batteryLevel: Int = 0
	private var batteryAC: String? = null
	private var batteryStatus: String? = null
	private var batteryHealth: String? = null
	private val batteryInfoReceiver = object : BroadcastReceiver() {
		override fun onReceive(c: Context, intent: Intent) {
			batteryAC = plugged[intent.getIntExtra("plugged", 0)]
			batteryStatus = state[intent.getIntExtra("status", 4)] // Default is "Not Charging"
			batteryHealth = health[intent.getIntExtra("health", 2)] // Default is "Good"
			present = intent.getIntExtra("status", 1) != 0
			batteryLevel = intent.getIntExtra("level", 0)
		}
	}

	private lateinit var pending: PendingIntent

	init {
		try {
			flush("Android Console: type 'help' for a list of commands\r\n$OK")

		} catch (e: Exception) {
			throw e
		}

		state[BatteryManager.BATTERY_STATUS_UNKNOWN] = "Unknown"
		state[BatteryManager.BATTERY_STATUS_CHARGING] = "Charging"
		state[BatteryManager.BATTERY_STATUS_DISCHARGING] = "Discharging"
		state[BatteryManager.BATTERY_STATUS_NOT_CHARGING] = "Not Charging"
		state[BatteryManager.BATTERY_STATUS_FULL] = "Full"

		health[BatteryManager.BATTERY_HEALTH_UNKNOWN] = "Unknown"
		health[BatteryManager.BATTERY_HEALTH_GOOD] = "Good"
		health[BatteryManager.BATTERY_HEALTH_OVERHEAT] = "Overheat"
		health[BatteryManager.BATTERY_HEALTH_DEAD] = "Dead"
		health[BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE] = "Over Voltage"
		health[BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE] = "Unspecified Failure"

		plugged[0] = "offline"
		plugged[BatteryManager.BATTERY_PLUGGED_USB] = "online"
		plugged[BatteryManager.BATTERY_PLUGGED_AC] = plugged[BatteryManager.BATTERY_PLUGGED_USB]

		startListeners()

		app.registerReceiver(batteryInfoReceiver, IntentFilter(ACTION_BATTERY_CHANGED))

		this.sendIntent = Intent(Intent.ACTION_VIEW)
		this.sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

		configureNotification()
	}

	private fun configureNotification() {
		notification
			.setAutoCancel(true)
			.setSmallIcon(R.drawable.sms)
			.setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
			.setLights(Color.RED, 3000, 3000)
			.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
	}

	override fun respond(line: String): Boolean {
		val resp = StringBuffer()

		val tokens = line.split("\\s+".toRegex()) //.dropLastWhile { it.isEmpty() }.toTypedArray()
		val NUM_TOKEN = tokens.size
		logger.log(INFO, tokens.toString())

		when (tokens[0]) {
			"help", "h", "?" -> {
				if (NUM_TOKEN == 1)
					respondSingleHelp(resp)
				else
					respondSpecificHelp(tokens[1], resp)
			}
			"help-verbose" -> respondSingleHelpVerbose(resp)
			"ping" -> resp.append("I am alive!\r\n$OK")
			"event" -> {
				if (NUM_TOKEN == 1) {
					resp.append("allows you to send fake hardware events to the kernel\r\n\n")
					resp.append(SUB_COMMANDS)
					resp.append("    send             send a series of events to the kernel\r\n")
					resp.append("    types            list all <type> aliases\r\n")
					resp.append("    codes            list all <code> aliases for a given <type>\r\n")
					resp.append("    text             simulate keystrokes from a given text\r\n\n")
					resp.append("    mouse            simulate a mouse event\r\n\n")
					resp.append(MISS_SUB_COMMANDS)
				} else when (tokens[1]) {
					"send" -> {
						if (NUM_TOKEN == 3) {
							when (tokens[2]) {
								"EV_SYN", "EV_KEY", "EV_REL", "EV_ABS", "EV_MSC", "EV_SW", "EV_LED", "EV_SND", "EV_REP", "EV_FF", "EV_PWR", "EV_FF_STATUS", "EV_MAX" -> resp.append(OK)
								else -> resp.append("KO: invalid event type in '${tokens[2]}', try 'event list types' for valid values\r\n")
							}
						} else
							resp.append("KO: Usage: event send <type>:<code>:<value> ...\r\n")
					}
					"types" -> {
						resp.append("event <type> can be an integer or one of the following aliases\r\n")
						resp.append("    EV_SYN\r\n")
						resp.append("    EV_KEY    (405 code aliases)\r\n")
						resp.append("    EV_REL    (2 code aliases)\r\n")
						resp.append("    EV_ABS    (27 code aliases)\r\n")
						resp.append("    EV_MSC\r\n")
						resp.append("    EV_SW     (4 code aliases)\r\n")
						resp.append("    EV_LED\r\n")
						resp.append("    EV_SND\r\n")
						resp.append("    EV_REP\r\n")
						resp.append("    EV_FF\r\n")
						resp.append("    EV_PWR\r\n")
						resp.append("    EV_FF_STATUS\r\n")
						resp.append("    EV_MAX\r\n")
						resp.append(OK)
					}
					"codes" -> {
						if (NUM_TOKEN >= 3) respondEventCodes(tokens[2], resp)
						else
							resp.append("KO: argument missing, try \'event codes <types>\'\r\n")
					}
					"text" -> {
						if (NUM_TOKEN == 3) {
							/*Build.VERSION_CODES.HONEYCOMB
							clipboard?.primaryClip = ClipData.newPlainText("Texto recebido:${tokens[2]}", tokens[2])
							val item = clipboard?.getPrimaryClip()?.getItemAt(0)
							item!!.text*/
							// TODO implementar 'Paste' https://stackoverflow.com/a/41721358/3443949
							resp.append(OK)
						} else
							resp.append("KO: argument missing, try \'event text <message>\'\r\n")
					}
					"mouse" -> { //TODO simular evento de toque
					}
				}
			}
			"geo" -> {
				if (NUM_TOKEN == 1){
					resp.append("allows you to change Geo-related settings, or to send GPS NMEA sentences\r\n\n")
					resp.append(SUB_COMMANDS)
					resp.append("    nmea             send an GPS NMEA sentence\n")
					resp.append("    fix              send a simple GPS fix\r\n\n")
					resp.append(MISS_SUB_COMMANDS)
				} else {
					when (tokens[1]) { // TODO implementar 'geo'
						"nmea", "fix" -> resp.append(OK)
						else -> resp.append(MISS_SUB_COMMANDS)
					}
				}
			}
			"gsm" -> {
				if (NUM_TOKEN == 1) {
					resp.append("allows you to change GSM-related settings, or to make a new inbound phone call\r\n\n")
					resp.append(SUB_COMMANDS)
					resp.append("    list             list current phone calls\r\n")
					resp.append("    call             create inbound phone call\r\n")
					resp.append("    busy             close waiting outbound call as busy\r\n")
					resp.append("    hold             change the state of an oubtound call to 'held'\r\n")
					resp.append("    accept           change the state of an outbound call to 'active'\r\n")
					resp.append("    cancel           disconnect an inbound or outbound phone call\r\n")
					resp.append("    data             modify data connection state\r\n")
					resp.append("    voice            modify voice connection state\r\n")
					resp.append("    status           display GSM status\r\n")
					resp.append(MISS_SUB_COMMANDS)

				} else when (tokens[1]) {
					"list" -> {
						val listCall = CallsProvider(app).calls.list.reversed()
						listCall.
							subList(0, if (listCall.size < 15) (listCall.size - 1) else 14).forEachIndexed { i, it ->
							resp.append("Call number    ${i+1}\r\n")
							resp.append("    name:      ${it.name}\r\n")
							resp.append("    number:    ${it.number}\r\n")
							resp.append("    type:      ${it.type}\r\n")
							resp.append("    date:      ${it.callDate}\r\n")
							resp.append("    duration:  ${it.duration}\r\n\n")
						}
						resp.append(OK)
					}
					"call" -> {
						if (NUM_TOKEN >= 3) {
							app.contentResolver.insert(Call.uri,
								ContentValues().apply {
									put(Calls.NUMBER, tokens[2])
									put(Calls.CACHED_NAME, "")
									put(Calls.DATE, System.currentTimeMillis())
									put(Calls.DURATION, 30)
									put(Calls.TYPE, Calls.INCOMING_TYPE)
									put(Calls.NEW, 1)
									put(Calls.CACHED_NUMBER_TYPE, 0)
									put(Calls.CACHED_NUMBER_LABEL, "")
								}
							)
							resp.append(OK)
						} else resp.append("KO: Usage: gsm call <phonenumber>")

					}
					"busy", "hold", "accept", "cancel", "data", "voice", "signal", "signal-profile" -> resp.append(OK)
					"status" -> {} //TODO mostrar o último status modificado 'voice' e 'data'
					else -> resp.append(MISS_SUB_COMMANDS)
				}
			}
			/*"cdma" -> {
				if (NUM_TOKEN == 1) {
					outter.append("allows you to change CDMA-related settings\r\n\n")
					outter.append(SUB_COMMANDS)
					outter.append("    ssource          Set the current CDMA subscription source\r\n")
					outter.append("    prl_version      Dump the current PRL version\r\n")
					outter.append(MISS_SUB_COMMANDS)
				} else when (tokens[1]) {
					"ssource", "prl_version" -> outter.append(OK)
					else -> outter.append(MISS_SUB_COMMANDS)
				}
			}*/
			"crash" -> resp.append("OK: crashing emulator, bye bye\r\n")
			"crash-on-exit" -> resp.append("OK: crashing emulator on exit, bye bye\r\n")
			"kill" -> resp.append("kill the device\r\n$OK")
			"network" -> {
				if (NUM_TOKEN == 1) {
					resp.append("allows you to manage the settings related to the network data connection of the emulated device.\r\n\n")
					resp.append(SUB_COMMANDS)
					resp.append("    status           retrieve network status\r\n")
					resp.append("    wifi             change wifi status\r\n")
					resp.append(MISS_SUB_COMMANDS)
				} else when (tokens[1]) {
					"status" -> {
						resp.append("  status:        ${Connectivity.getStatus(app)}\r\n")
						val wifi = if (Connectivity.isConnectedWifi(app)) "enabled" else "disabled"
						resp.append("  wifi:          $wifi\r\n")
						val mobile = if (Connectivity.isConnectedMobile(app)) "enabled" else "disabled"
						resp.append("  mobile data:   $mobile\r\n")
						resp.append(OK)
					}
					"wifi" -> when (tokens[2]) {
						"true", "false" -> {wifiManager.isWifiEnabled = tokens[2].toBoolean(); resp.append(OK)}
						else -> {
							resp.append("allows to enable/disable the device's wifi\r\n\n")
							resp.append(SUB_COMMANDS)
							resp.append("    true      enable wifi\n")
							resp.append("    false     disable wifi\r\n")
							resp.append(BAD_SUB_COMMAND)
						}
					}
					/*"mobile" -> { // https://stackoverflow.com/a/31488730/3443949
						when(tokens[2]) {
							"true", "false" -> { setDataEnabled(tokens[2].toBoolean()); outter.append(OK) }
							else -> {
								outter.append("allows to enable/disable the device's mobile data\r\n\n")
								outter.append(SUB_COMMANDS)
								outter.append("    true      enable data connectivity\n")
								outter.append("    false     disable data connectivity\r\n")
								outter.append(BAD_SUB_COMMAND)
							}
						}
					}*/
					else -> {
						resp.append("allows you to manage the settings related to the network data connection of the emulated device.\r\n\n")
						resp.append(SUB_COMMANDS)
						resp.append("    status           retrieve network status\r\n")
						resp.append("    wifi             change wifi status\r\n")
						resp.append(BAD_SUB_COMMAND)
					}
				}
			}
			"power" -> {
				if (NUM_TOKEN == 1) {
					resp.append("allows get battery and batteryAC power status\r\n\n")
					resp.append(SUB_COMMANDS)
					resp.append("   power display          display battery and charger state\r\n\n")
					resp.append(MISS_SUB_COMMANDS)
				} else when (tokens[1]) {
					"display" -> displayPower(resp)
					else -> {
						resp.append("allows to change battery and batteryAC power status\r\n\n")
						resp.append(SUB_COMMANDS)
						resp.append("   power display          display battery and charger state\r\n\n")
						resp.append(BAD_SUB_COMMAND)
					}
				}
				resp.append(OK)
			}
			"quit", "exit" -> { stopListeners(); return true}
			"redir" -> { // TODO https://stackoverflow.com/questions/4658619/how-to-use-iptables-in-an-android-application
				if (NUM_TOKEN == 1) {
					resp.append("allows you to add, list and remove UDP and/or PORT redirection from the host to the device as an example, 'redir  tcp:5000:6000' will route any packet sent to the host's TCP port 5000 to TCP port 6000 of the emulated device\r\n\r")
					resp.append(SUB_COMMANDS)
					resp.append("    list             list current redirections\r\n")
					resp.append("    add              add new redirection\r\n")
					resp.append("    del              remove existing redirection\r\n\n")
					resp.append(MISS_SUB_COMMANDS)
				} else when (tokens[1]) {
					"list", "add", "del" -> resp.append(OK) // TODO gerenciar redirecionamentos
					else -> {
						resp.append("allows you to add, list and remove UDP and/or PORT redirection from the host to the device as an example, 'redir  tcp:5000:6000' will route any packet sent to the host's TCP port 5000 to TCP port 6000 of the emulated device\r\n\r")
						resp.append(SUB_COMMANDS)
						resp.append("    list             list current redirections\r\n")
						resp.append("    add              add new redirection\r\n")
						resp.append("    del              remove existing redirection\r\n\n")
						resp.append(BAD_SUB_COMMAND)
					}
				}
			}
			"sms" -> { // TODO sms não está sendo enviado, apenas a notificação
				if (NUM_TOKEN == 1) {
					resp.append("allows you to simulate an inbound SMS\r\n\n")
					resp.append(SUB_COMMANDS)
					resp.append("    send             send inbound SMS text message\r\n")
					resp.append("    pdu              send inbound SMS PDU\r\n\n")
					resp.append(MISS_SUB_COMMANDS)
				} else if (NUM_TOKEN < 4) when(tokens[1]) {
					"list" -> {
						val list = TelephonyProvider(app).getSms(TelephonyProvider.Filter.ALL).list
						list.subList(0, if (list.size < 15) (list.size - 1) else 14).forEachIndexed { i, sms ->
							resp.append("SMS number     ${i+1}\r\n")
							resp.append("    number:    ${sms.address}\r\n")
							resp.append("    message:   ${sms.body}\r\n")
							resp.append("    received:  ${sms.receivedDate}\r\n\n")
						}
						resp.append(OK)
					}
					"send" ->
						resp.append("KO: missing argument, try 'sms send <phonenumber> <text message>'\r\n")
					"pdu" -> resp.append("KO: badly formatted <hexstring>\r\n")
					else -> {
						resp.append("allows you to simulate an inbound SMS\r\n\n")
						resp.append(SUB_COMMANDS)
						resp.append("    send             send inbound SMS text message\r\n")
						resp.append("    pdu              send inbound SMS PDU\r\n\n")
						resp.append(BAD_SUB_COMMAND)
					}
				} else when (tokens[1]) {
					"tt" -> { // todo retirar caso não funcione totalmente
						app.contentResolver.insert(me.everything.providers.android.telephony.Sms.uri,
							ContentValues().apply {
								put(Telephony.TextBasedSmsColumns.ADDRESS, "+55981145678")
								put(Telephony.TextBasedSmsColumns.BODY, "mensagem")
								put(Telephony.TextBasedSmsColumns.DATE, System.currentTimeMillis())
								put(Telephony.TextBasedSmsColumns.READ, 0) // 0 não lido
							})
						resp.append(OK)
					}
					"send" -> { // referente ao comando: SEND
						val address = tokens[2]

						val pattern = Pattern.compile("[+]?[0-9]*")

						if (pattern.matcher(address).matches()) {
							val bodyStart = line.indexOf(address) + address.length
							val body = line.substring(bodyStart)

							// create the SMS:
							this.createSMS(address, body, body.length > TextBasedSmsColumns.LIMIT_TICKER)

							//							this.saveSMS(address, body)

							resp.append(OK)
						} else
							resp.append("KO: bad phone number format, must be [+](0-9)*\r\n")
					}
				}
			}
			/*"avd" -> {
				if (NUM_TOKEN == 1) {
					outter.append("allows you to control (e.g. start/stop) the execution of the virtual device\r\n\r")
					outter.append(SUB_COMMANDS)
					outter.append("    stop             stop the virtual device\r\n")
					outter.append("    start            start/restart the virtual device\r\n")
					outter.append("    status           query virtual device status\r\n")
					outter.append("    name             query virtual device name\r\n")
					outter.append("    snapshot         state snapshot commands\r\n\n")
					outter.append(MISS_SUB_COMMANDS)
				} else when (tokens[1]) {
					"stop", "start" -> outter.append(OK) // TODO implementar stop
					"status" -> outter.append("device is running\n${OK}")
					"name" -> {} // TODO resgatar nome do dispositivo
					"snapshot" -> {} // TODO implementar 'avd snapshot'
					else -> {
						outter.append("allows you to control (e.g. start/stop) the execution of the virtual device\r\n\r")
						outter.append(SUB_COMMANDS)
						outter.append("    stop             stop the virtual device\r\n")
						outter.append("    start            start/restart the virtual device\r\n")
						outter.append("    status           query virtual device status\r\n")
						outter.append("    name             query virtual device name\r\n")
						outter.append("    snapshot         state snapshot commands\r\n\n")
						outter.append(BAD_SUB_COMMAND)
					}
				}
			}*/
			/*"qemu" -> {
				if (NUM_TOKEN == 1) {
					outter.append("allows to connect to the QEMU virtual machine monitor\r\n\r")
					outter.append(SUB_COMMANDS)
					outter.append("    monitor          enter QEMU monitor\r\n\n")
					outter.append(MISS_SUB_COMMANDS)
				} else when (tokens[1]) {
					"monitor" -> outter.append("KO: QEMU support no longer available")
					else -> {
						outter.append("allows to connect to the QEMU virtual machine monitor\r\n\r")
						outter.append(SUB_COMMANDS)
						outter.append("    monitor          enter QEMU monitor\r\n\n")
						outter.append(BAD_SUB_COMMAND)
					}
				}
			}*/
			"sensor" -> {
				if (NUM_TOKEN == 1) {
					resp.append("allows you to request the device sensors\r\n\n")
					resp.append(SUB_COMMANDS)
					resp.append("    list             list all sensors\r\n")
					resp.append("    get              get sensor values\r\n")
					resp.append(MISS_SUB_COMMANDS)
				} else when (tokens[1]) {
					"list" -> {
						resp.append("  acceleration$RN")
						resp.append("  gyroscope").append(if (gyroStatus) RN else " not available$RN")
						resp.append("  magnetic-field$RN")
						resp.append("  orientation$RN")
						resp.append("  temperature$RN")
						resp.append("  proximity").append(if (proxStatus) RN else " not available$RN")
						resp.append("  light$RN")
						resp.append("  pressure$RN")
						resp.append("  humidity$RN")
						resp.append(OK)
					}
					"get" -> if (NUM_TOKEN == 3) {
						when (tokens[2]) { // TODO obter dados de sensor
							"acceleration" -> resp.append("${tokens[2]} - $accel$RN").append(OK)
							"gyroscope" -> resp.append("${tokens[2]} - $gyro$RN").append(OK)
							"magnetic-field" -> resp.append("${tokens[2]} - $magn$RN").append(OK)
							"orientation" -> resp.append("${tokens[2]} = $orient$RN").append(OK)
							"temperature" -> resp.append("${tokens[2]} = $temp\r\n").append(OK) // todo deu erro
							"proximity" -> resp.append("${tokens[2]} - $distance\r\n").append(OK)
							"light" -> resp.append("${tokens[2]} - $lux\r\n").append(OK)
							"pressure" -> resp.append("${tokens[2]} - $pressure\r\n").append(OK)
							"humidity" -> resp.append("${tokens[2]} - $humi\r\n").append(OK)
							else -> resp.append("KO: unknown sensor name:${tokens[2]}, run 'sensor status' to get available sensors.\r\n")
						}
					}
					else -> resp.append(BAD_SUB_COMMAND)
				}
			}
			/*"physics" -> {
				if (NUM_TOKEN == 1) {
					outter.append("allows you to record and playback physical model state changes\r\n\n")
					outter.append(SUB_COMMANDS)
					outter.append("    record           start recording physical state changes.\r\n")
					outter.append("    play             start playing physical state changes.\r\n")
					outter.append("    stop             stop recording or playing back physical state changes.\r\n\n")
					outter.append(MISS_SUB_COMMANDS)
				} else when (tokens[1]) {
					"record", "play" -> outter.append("\r\n")
					"stop" -> outter.append(OK)
					else -> {
						outter.append("allows you to record and playback physical model state changes\r\n\n")
						outter.append(SUB_COMMANDS)
						outter.append("    record           start recording physical state changes.\r\n")
						outter.append("    play             start playing physical state changes.\r\n")
						outter.append("    stop              stop recording or playing back physical state changes.\r\n\n")
						outter.append(BAD_SUB_COMMAND)
					}
				}
			}*/
			"finger" -> {
				if (NUM_TOKEN == 1) {
					resp.append("allows you to touch the emulator finger print sensor\r\n\n")
					resp.append(SUB_COMMANDS)
					resp.append("    touch           touch finger print sensor with <fingerid>\r\n")
					resp.append("    remove             remove finger from the fingerprint sensor\r\n\n")
					resp.append(MISS_SUB_COMMANDS)
				} else when (tokens[1]) {
					"touch" -> {
						if (NUM_TOKEN == 2) resp.append("KO: missing fingerid\r\n")
						else {
							if (tokens[2].matches("-?\\d+(\\.\\d+)?".toRegex()))
								resp.append(OK)
							else resp.append("KO: invalid fingerid\r\n")
						}
					}
					"remove" -> resp.append(OK)
					else -> {
						resp.append("allows you to touch the emulator finger print sensor\r\n\n")
						resp.append(SUB_COMMANDS)
						resp.append("    touch           touch finger print sensor with <fingerid>\r\n")
						resp.append("    remove             remove finger from the fingerprint sensor\r\n\n")
						resp.append(BAD_SUB_COMMAND)
					}
				}
			}
//			"debug" -> {}
			/*"rotate" -> {
				setAutorotate(tokens[1].toBoolean())// todo verificação da formatação do comando
				outter.append(OK)
			}*/
			"screenshot" -> { // TODO revisar captura
				val process: Process?
				try {
					val streamPath = Environment.getExternalStorageDirectory().path + "/teste.png"
					logger.log(WARN, "path capture: $streamPath")
					process = Runtime.getRuntime().exec("screencap -p $streamPath")
					if (process.waitFor() == 1) resp.append(OK)
				} catch (e: InterruptedException) { }
			}
			/*"screenrecord" -> {
				if (NUM_TOKEN == 1) {
					outter.append("Records the emulator's display\r\n\n")
					outter.append(SUB_COMMANDS)
					outter.append("    start            start screen recording\r\n")
					outter.append("    stop             stop screen recording\r\n")
					outter.append("    screenshot       Take a screenshot\r\n")
					outter.append("    webrtc           start/stop the webrtc module\r\n\n")
					outter.append(MISS_SUB_COMMANDS)
				} else when (tokens[1]) {
					"start", "stop" -> {}
					"screenshot" -> {} // TODO obter captura de tela
					"webrtc" -> {} // TODO descobrir pra que serve essa droga
					else -> {
						outter.append("Records the emulator's display\r\n\n")
						outter.append(SUB_COMMANDS)
						outter.append("    start            start screen recording\r\n")
						outter.append("    stop             stop screen recording\r\n")
						outter.append("    screenshot       Take a screenshot\r\n")
						outter.append("    webrtc           start/stop the webrtc module\r\n\n")
						outter.append(BAD_SUB_COMMAND)
					}
				}
			}*/
			else -> resp.append("KO: unknown command, try 'help'\r\n")
		}

		flush(resp.toString())

		return false
	}

	private fun displayPower(saida: StringBuffer) {
		saida.append("batteryAC: $batteryAC\r\n")
		saida.append("status: $batteryStatus\r\n")
		saida.append("health: $batteryHealth\r\n")
		saida.append("present: $present\r\n")
		saida.append("capacity: $batteryLevel\r\n")
		saida.append(OK)
	}

	/**
	 * Simulates a received sms
	 *
	 * @param address
	 * Sender address
	 * @param body
	 * Message body
	 * @param tickerForBody
	 * **True** if is necessary to extract the tickerText of the message body
	 */
	private fun createSMS(address: String, body: String, tickerForBody: Boolean) {
		var thread_id: Long = -1
		val smsResolver = app.contentResolver

//		android.provider.Telephony.Sms.Inbox.addMessage(smsResolver, address, body, null, System.currentTimeMillis(), false)

		Inbox.addMessage(smsResolver, address, body, null, System.currentTimeMillis(), false)
		//TODO		logger.log(MSGTYPE.DEBUG, "mensagem inserida");
		// retrieves the id of the inserted sms:
		val whereClause = "${TextBasedSmsColumns.ADDRESS}=$address"
		val cursor = Sms.query(smsResolver, TextBasedSmsColumns.THREAD_ID, whereClause)

		try {
			if (cursor != null) {
				cursor.moveToFirst()
				thread_id = cursor.getLong(cursor.position)
			}

		} catch (e: CursorIndexOutOfBoundsException) {
			logger.log(ERROR, e.message!!)
		}

		logger.log(INFO, "MENSAGEM ENVIADA | ID: $thread_id")

		// notifies the message:
		pending = PendingIntent.getActivity(app, 0, setData(sendIntent, thread_id), 0)

		/*		notification.tickerText = address + ": "
						+ (tickerForBody ? body.substring(0, Sms.LIMIT_TICKER) : body);
				notification.when = System.currentTimeMillis();
				notification.setLatestEventInfo(context, address, body, pending);

				NotificationCompat.Builder builder = (Builder) new Builder(context)
					.setSmallIcon(R.drawable.sms)*/
		notification
			.setTicker(address + ": " + if (tickerForBody) body.substring(0, TextBasedSmsColumns.LIMIT_TICKER) else body)
			.setContentTitle(address)
			.setContentText(body)
			.setPriority(NotificationCompat.PRIORITY_DEFAULT)
			.setContentIntent(pending)
			.setWhen(System.currentTimeMillis())
		//			.setAutoCancel(true)
		//			.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

		notificationManager.notify(notificationId, notification.build())
	}

	/*private fun saveSMS(phoneNumber: String, body: String, readState: String = "0"): Boolean {
		var ret = false

		try {
			/*val values = ContentValues().apply {
				put("address", phoneNumber)
				put("body", body)
				put("read", readState) //"0" for have not read sms and "1" for have read sms
				put("date", System.currentTimeMillis())
			}

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
				var uri = Telephony.Sms.Sent.CONTENT_URI
				uri = Telephony.Sms.Inbox.CONTENT_URI;
				app.contentResolver.insert(uri, values)
			} else
				app.contentResolver.insert(Uri.parse("content://sms/inbox"), values)

			ret = true*/

			val smgr = SmsManager.getDefault()
			smgr.sendTextMessage("+38091234567", null, body, null, null)

		} catch (ex: Exception) {
			ret = false
		}

		return (ret)
	}*/

	private fun setData(intent: Intent, thread_id: Long): Intent =
		(intent.setData(ContentUris.withAppendedId(Sms.CONTENT_CONVERSATION, thread_id)))

	/**
	 * The event Types' code aliases
	 *
	 * @param type The type to get the codes
	 * @param saida The output data of the Help service
	 */
	private fun respondEventCodes(type: String, saida: StringBuffer) {
		when(type) {
			"EV_KEY" -> {
				saida.append("type \'EV_KEY\' accepts the following <code> aliases:\r\n")
				saida.append(EV_KEY + OK)
			}
			"EV_REL" -> {
				saida.append("type \'EV_REL\' accepts the following <code> aliases:\r\n")
				saida.append("    REL_X\r\n")
				saida.append("    REL_Y\r\n")
				saida.append(OK)
			}
			"EV_ABS" -> {
				saida.append("type \'EV_ABS\' accepts the following <code> aliases:\r\n")
				saida.append("    ABS_X\n")
				saida.append("    ABS_Y\n")
				saida.append("    ABS_Z\n")
				saida.append("    ABS_RX\n")
				saida.append("    ABS_RY\n")
				saida.append("    ABS_RZ\n")
				saida.append("    ABS_THROTTLE\n")
				saida.append("    ABS_RUDDER\n")
				saida.append("    ABS_WHEEL\n")
				saida.append("    ABS_GAS\n")
				saida.append("    ABS_BRAKE\n")
				saida.append("    ABS_HAT0X\n")
				saida.append("    ABS_HAT0Y\n")
				saida.append("    ABS_HAT1X\n")
				saida.append("    ABS_HAT1Y\n" +
					"    ABS_HAT2X\n    ABS_HAT2Y\n" +
					"    ABS_HAT3X\n    ABS_HAT3Y\n" +
					"    ABS_PRESSURE\n    ABS_DISTANCE\n" +
					"    ABS_TILT_X\n    ABS_TILT_Y\n" +
					"    ABS_TOOL_WIDTH\n    ABS_VOLUME\n" +
					"    ABS_MISC\n    ABS_MAX\r\n")
				saida.append(OK)
			}
			"EV_SW" -> {
				saida.append("type \'EV_SW\' accepts the following <code> aliases:\r\n")
				saida.append("    SW_LID\r\n")
				saida.append("    SW_TABLET_MODE\r\n")
				saida.append("    SW_HEADPHONE_INSERT\r\n")
				saida.append("    SW_MICROPHONE_INSER\r\n")
				saida.append(OK)
			}
			"EV_SYN", "EV_MSC", "EV_LED", "EV_SND", "EV_REP", "EV_FF", "EV_PWR", "EV_FF_STATUS", "EV_MAX" -> saida.append("no code aliases defined for this type\r\n" + OK)
			else -> saida.append("KO: bad argument, see \'event types\' for valid values\r\n")
		}
	}

	/**
	 * The simple help's menu of the protocol Telnet
	 *
	 * @param saida The output data of the Help service
	 */
	private fun respondSingleHelp(saida: StringBuffer) {
		saida.append("Android console command help:\r\n\n")
		saida.append("    help|h|?\r\n")
		saida.append("    help-verbose\r\n")
		saida.append("    ping\r\n")
		saida.append("    event\r\n")
		saida.append("    geo\r\n")
		saida.append("    gsm\r\n")
//		saida.append("    cdma\r\n")
		saida.append("    crash\r\n")
		saida.append("    crash-on-exit\r\n")
		saida.append("    kill\r\n")
		saida.append("    network\r\n")
		saida.append("    power\r\n")
		saida.append("    quit|exit\r\n")
		saida.append("    redir\r\n")
		saida.append("    sms\r\n")
		/*saida.append("    avd\r\n")
		saida.append("    qemu\r\n")*/
		saida.append("    sensor\r\n")
		/*saida.append("    physics\r\n")
		saida.append("    finger\r\n")*/
		saida.append("    screenshot\r\n\n")
		/*saida.append("    debug\r\n")
		saida.append("    rotate\r\n")
		saida.append("    screenrecord\r\n\n")*/

		saida.append("Try 'help-verbose' for more description\r\n")
		saida.append("Try 'help <command>' for command-specific help\r\n${OK}")
	}

	/**
	 * The verbose help's menu of the protocol Telnet
	 *
	 * @param saida
	 * The output data of the Help service
	 */
	private fun respondSingleHelpVerbose(saida: StringBuffer) {
		saida.append("Android console command help:\r\n\n")
		saida.append("    help|h|?         print a list of commands\r\n")
		saida.append("    help-verbose     print a list of commands with descriptions\r\n")
		saida.append("    ping             check if the emulator is alive\r\n")
		saida.append("    event            simulate hardware events\r\n")
		saida.append("    geo              Geo-location commands\r\n")
		saida.append("    gsm              GSM related commands\r\n")
//		saida.append("    cdma             CDMA related commands\r\n")
		saida.append("    crash            crash the emulator instance\r\n")
		saida.append("    crash-on-exit    simulate crash on exit for the emulator instance\r\n")
		saida.append("    kill             kill the emulator instance\r\n")
		saida.append("    network          manage network settings\r\n")
		saida.append("    power            power related commands\r\n")
		saida.append("    quit|exit        quit control session\r\n")
		saida.append("    redir            manage port redirections\r\n")
		saida.append("    sms              SMS related commands\r\n")
		/*saida.append("    avd              control virtual device execution\r\n")
		saida.append("    qemu             QEMU-specific commands\r\n")*/
		saida.append("    sensor           manage emulator sensors\r\n")
//		saida.append("    physics          manage physical model\r\n")
		saida.append("    finger           manage emulator finger print\r\n")
		saida.append("    screenschot      saves an screenshot\r\n\n")
		/*saida.append("    debug            control the emulator debug output tags\r\n")
		saida.append("    rotate           rotate the screen clockwise by 90 degrees\r\n")
		saida.append("    screenrecord     Records the emulator's display\r\n\n")*/

		saida.append("try 'help <command>' for command-specific help\r\n$OK")
	}

	private fun respondSpecificHelp(token: String, saida: StringBuffer) {
		when (token) {
			"help", "h", "?" -> {

			}
			"event" -> {
				saida.append("allows you to send fake hardware events to the kernel\r\n\n")
				saida.append(SUB_COMMANDS)
				saida.append("   event send             send a series of events to the kernel\r\n")
				saida.append("   event types            list all <type> aliases\r\n")
				saida.append("   event codes            list all <code> aliases for a given <type>\r\n")
				saida.append("   event codes            list all <code> aliases for a given <type>\r\n\n")
				saida.append(OK)
			}
			"geo" -> {
				saida.append("allows you to change Geo-related settings, or to send GPS NMEA sentences\r\n\n")
				saida.append(SUB_COMMANDS)
				saida.append("    geo nmea             send an GPS NMEA sentence\r\n")
				saida.append("    geo fix              send a simple GPS fix\r\n")
				saida.append(OK)
			}
			"gsm" -> {
				saida.append("allows you to change GSM-related settings, or to make a new inbound phone call\r\n\n")
				saida.append(SUB_COMMANDS)
				saida.append("    gsm list             list current phone calls\r\n")
				saida.append("    gsm call             create inbound phone call\r\n")
				saida.append("    gsm busy             close waiting outbound call as busy\r\n")
				saida.append("    gsm hold             change the state of an oubtound call to 'held'\r\n")
				saida.append("    gsm accept           change the state of an outbound call to 'active'\r\n")
				saida.append("    gsm cancel           disconnect an inbound or outbound phone call\r\n")
				saida.append("    gsm data             modify data connection state\r\n")
				saida.append("    gsm voice            modify voice connection state\r\n")
				saida.append("    gsm status           display GSM status\r\n")
				saida.append("    gsm signal           set sets the rssi and ber\r\n\n")
				saida.append(OK)
			}
			/*"cdma" -> {
				saida.append("allows you to change CDMA-related settings\r\n\n")
				saida.append(SUB_COMMANDS)
				saida.append("    cdma ssource          Set the current CDMA subscription source\r\n")
				saida.append("    cdma prl_version      Dump the current PRL version\r\n")
				saida.append(OK)
			}*/
			"kill" -> saida.append("kill the device\r\n$OK")
			"network" -> {
				saida.append("allows you to manage the settings related to the network data connection of the emulated device.\r\n\n")
				saida.append(SUB_COMMANDS)
				saida.append("    network status           retrieve network status\r\n")
				saida.append("    network wifi             change wifi status\r\n")
				saida.append(OK)
			}
			"power" -> {
				saida.append("allows to view battery and batteryAC power status\r\n\n")
				saida.append(SUB_COMMANDS)
				saida.append("   power display          display battery and charger state\r\n\n")
				saida.append(OK)
			}
			"quit", "exit" -> saida.append("quit control session\r\n" + OK)
			"redir" -> {
				saida.append("allows you to add, list and remove UDP and/or PORT redirection from the host to the device as an example, 'redir  tcp:5000:6000' will route any packet sent to the host's T CP port 5000 to TCP port 6000 of the emulated device\r\n\r")
				saida.append(SUB_COMMANDS)
				saida.append("    redir list             list current redirections\r\n")
				saida.append("    redir add              add new redirection\r\n")
				saida.append("    redir del              remove existing redirection\r\n")
				saida.append(OK)
			}
			"sms" -> {
				saida.append("allows you to simulate an inbound SMS\r\n\n")
				saida.append(SUB_COMMANDS)
				saida.append("   sms send             send inbound SMS text message\r\n")
				saida.append("   sms pdu              send inbound SMS PDU\r\n\n")
				saida.append(OK)
			}
			/*"avd" -> {
				saida.append("allows you to control (e.g. start/stop) the execution of the virtual device\r\n\r")
				saida.append(SUB_COMMANDS)
				saida.append("    avd stop             stop the virtual device\r\n")
				saida.append("    avd start            start/restart the virtual device\r\n")
				saida.append("    avd status           query virtual device status\r\n")
				saida.append("    avd name             query virtual device name\r\n")
				saida.append("    avd snapshot         state snapshot commands\r\n")
				saida.append(OK)
			}*/
			"sensor" -> {
				saida.append("allows you to request the emulator sensors\r\n\n")
				saida.append(SUB_COMMANDS)
				saida.append("    sensor status           list all sensors\r\n")
				saida.append("    sensor get              get sensor values\r\n")
				saida.append(OK)
			}
			else -> {
				saida.append("try one of these instead:\r\n\n")
				saida.append("    help|h|?\r\n")
				saida.append("    help-verbose\r\n")
				saida.append("    ping\r\n")
				saida.append("    event\r\n")
				saida.append("    geo\r\n")
				saida.append("    gsm\r\n")
				saida.append("    crash\r\n")
				saida.append("    crash-on-exit\r\n")
				saida.append("    kill\r\n")
				saida.append("    network\r\n")
				saida.append("    power\r\n")
				saida.append("    quit|exit\r\n")
				saida.append("    redir\r\n")
				saida.append("    sms\r\n")
				saida.append("    sensor\r\n")
				saida.append("KO: unknown command\r\n")
			}
		}
	}

	/** Create the NotificationChannel, but only on API 26+ because
	 * the NotificationChannel class is new and not in the support library */
	private fun createNotificationChannel() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			val name = app.getString(R.string.channel_name)
			val description = app.getString(br.mario.labsac.honeypotframework.R.string.channel_description)
			val importance = NotificationManager.IMPORTANCE_DEFAULT
			val channel = NotificationChannel(CHANNEL_ID, name, importance)
			channel.description = description

			app.getSystemService(NotificationManager::class.java).createNotificationChannel(channel)
		}
	}
}

const val EV_KEY =
	"    KEY_ESC\n    KEY_1\n    KEY_2\n    KEY_3\n    KEY_4\n    KEY_5\n    KEY_6\n    KEY_7\n    KEY_8\n" +
		"    KEY_9\n    KEY_0\n    KEY_MINUS\n    KEY_EQUAL\n    KEY_BACKSPACE\n    KEY_TAB\n    KEY_Q\n    KEY_W\n" +
		"    KEY_E\n    KEY_R\n    KEY_T\n    KEY_Y\n    KEY_U\n    KEY_I\n    KEY_O\n    KEY_P\n" +
		"    KEY_LEFTBRACE\n    KEY_RIGHTBRACE\n    KEY_ENTER\n    KEY_LEFTCTRL\n    KEY_A\n    KEY_S\n" +
		"    KEY_D\n    KEY_F\n    KEY_G\n" +
		"    KEY_H\n    KEY_J\n    KEY_K\n    KEY_L\n    KEY_SEMICOLON\n    KEY_APOSTROPHE\n    KEY_GRAVE\n" +
		"    KEY_LEFTSHIFT\n    KEY_BACKSLASH\n    KEY_Z\n    KEY_X\n    KEY_C\n    KEY_V\n    KEY_B\n    KEY_N\n" +
		"    KEY_M\n    KEY_COMMA\n    KEY_DOT\n    KEY_SLASH\n    KEY_RIGHTSHIFT\n    KEY_KPASTERISK\n" +
		"    KEY_LEFTALT\n    KEY_SPACE\n    KEY_CAPSLOCK\n    KEY_F1\n    KEY_F2\n    KEY_F3\n    KEY_F4\n" +
		"    KEY_F5\n    KEY_F6\n    KEY_F7\n    KEY_F8\n    KEY_F9\n    KEY_F10\n    KEY_NUMLOCK\n" +
		"    KEY_SCROLLLOCK\n    KEY_KP7\n    KEY_KP8\n    KEY_KP9\n    KEY_KPMINUS\n    KEY_KP4\n    KEY_KP5\n" +
		"    KEY_KP6\n    KEY_KPPLUS\n    KEY_KP1\n    KEY_KP2\n    KEY_KP3\n    KEY_KP0\n    KEY_KPDOT\n" +
		"    KEY_ZENKAKUHANKAKU\n    KEY_102ND\n    KEY_F11\n    KEY_F12\n    KEY_RO\n    KEY_KATAKANA\n" +
		"    KEY_HIRAGANA\n    KEY_HENKAN\n    KEY_KATAKANAHIRAGAN\n    KEY_MUHENKAN\n    KEY_KPJPCOMMA\n" +
		"    KEY_KPENTER\n    KEY_RIGHTCTRL\n    KEY_KPSLASH\n    KEY_SYSRQ\n    KEY_RIGHTALT\n    KEY_LINEFEED\n" +
		"    KEY_HOME\n    KEY_UP\n    KEY_PAGEUP\n    KEY_LEFT\n    KEY_RIGHT\n    KEY_END\n    KEY_DOWN\n" +
		"    KEY_PAGEDOWN\n    KEY_INSERT\n    KEY_DELETE\n    KEY_MACRO\n    KEY_MUTE\n    KEY_VOLUMEDOWN\n" +
		"    KEY_VOLUMEUP\n    KEY_POWER\n    KEY_KPEQUAL\n    KEY_KPPLUSMINUS\n    KEY_PAUSE\n    KEY_KPCOMMA\n" +
		"    KEY_HANGEUL\n    KEY_HANJA\n    KEY_YEN\n    KEY_LEFTMETA\n    KEY_RIGHTMETA\n    KEY_COMPOSE\n" +
		"    KEY_STOP\n    KEY_AGAIN\n    KEY_PROPS\n    KEY_UNDO\n    KEY_FRONT\n    KEY_COPY\n    KEY_OPEN\n" +
		"    KEY_PASTE\n    KEY_FIND\n    KEY_CUT\n    KEY_HELP\n    KEY_MENU\n    KEY_CALC\n    KEY_SETUP\n" +
		"    KEY_SLEEP\n    KEY_WAKEUP\n    KEY_FILE\n    KEY_SENDFILE\n    KEY_DELETEFILE\n    KEY_XFER\n" +
		"    KEY_PROG1\n    KEY_PROG2\n    KEY_WWW\n    KEY_MSDOS\n    KEY_COFFEE\n    KEY_DIRECTION\n" +
		"    KEY_CYCLEWINDOWS\n    KEY_MAIL\n    KEY_BOOKMARKS\n    KEY_COMPUTER\n    KEY_BACK\n    KEY_FORWARD\n" +
		"    KEY_CLOSECD\n    KEY_EJECTCD\n    KEY_EJECTCLOSECD\n    KEY_NEXTSONG\n    KEY_PLAYPAUSE\n" +
		"    KEY_PREVIOUSSONG\n    KEY_STOPCD\n    KEY_RECORD\n    KEY_REWIND\n    KEY_PHONE\n    KEY_ISO\n" +
		"    KEY_CONFIG\n    KEY_HOMEPAGE\n    KEY_REFRESH\n    KEY_EXIT\n    KEY_MOVE\n    KEY_EDIT\n" +
		"    KEY_SCROLLUP\n    KEY_SCROLLDOWN\n    KEY_KPLEFTPAREN\n" +
		"    KEY_KPRIGHTPAREN\n    KEY_NEW\n    KEY_REDO\n    KEY_F13\n    KEY_F14\n" +
		"    KEY_F15\n    KEY_F16\n    KEY_F17\n    KEY_F18\n    KEY_F19\n    KEY_F20\n" +
		"    KEY_F21\n    KEY_F22\n    KEY_F23\n    KEY_F24\n    KEY_PLAYCD\n    KEY_PAUSECD\n    KEY_PROG3\n" +
		"    KEY_PROG4\n    KEY_SUSPEND\n    KEY_CLOSE\n    KEY_PLAY\n    KEY_FASTFORWARD\n    KEY_BASSBOOST\n" +
		"    KEY_PRINT\n    KEY_HP\n    KEY_CAMERA\n    KEY_SOUND\n    KEY_QUESTION\n    KEY_EMAIL\n    KEY_CHAT\n" +
		"    KEY_SEARCH\n    KEY_CONNECT\n    KEY_FINANCE\n    KEY_SPORT\n    KEY_SHOP\n    KEY_ALTERASE\n" +
		"    KEY_CANCEL\n    KEY_BRIGHTNESSDOWN\n    KEY_BRIGHTNESSUP\n    KEY_MEDIA\n    KEY_STAR\n    KEY_SHARP\n" +
		"    KEY_SOFT1\n    KEY_SOFT2\n    KEY_SEND\n    KEY_CENTER\n    KEY_HEADSETHOOK\n" +
		"    KEY_0_5\n    KEY_2_5\n    KEY_SWITCHVIDEOMODE\n    KEY_KBDILLUMTOGGLE\n" +
		"    KEY_KBDILLUMDOWN\n    KEY_KBDILLUMUP\n    KEY_REPLY\n    KEY_FORWARDMAIL\n    KEY_SAVE\n" +
		"    KEY_DOCUMENTS\n    KEY_BATTERY\n    KEY_UNKNOWN\n    KEY_NUM\n    KEY_FOCUS\n    KEY_PLUS\n" +
		"    KEY_NOTIFICATION\n    KEY_OK\n    KEY_SELECT\n    KEY_GOTO\n    KEY_CLEAR\n" +
		"    KEY_POWER2\n    KEY_OPTION\n    KEY_INFO\n    KEY_TIME\n    KEY_VENDOR\n    KEY_ARCHIVE\n" +
		"    KEY_PROGRAM\n    KEY_CHANNEL\n    KEY_FAVORITES\n    KEY_EPG\n    KEY_PVR\n    KEY_MHP\n" +
		"    KEY_LANGUAGE\n    KEY_TITLE\n    KEY_SUBTITLE\n    KEY_ANGLE\n    KEY_ZOOM\n    KEY_MODE\n" +
		"    KEY_KEYBOARD\n    KEY_SCREEN\n    KEY_PC\n    KEY_TV\n    KEY_TV2\n    KEY_VCR\n    KEY_VCR2\n" +
		"    KEY_SAT\n    KEY_SAT2\n    KEY_CD\n    KEY_TAPE\n    KEY_RADIO\n    KEY_TUNER\n    KEY_PLAYER\n" +
		"    KEY_TEXT\n    KEY_DVD\n    KEY_AUX\n    KEY_MP3\n    KEY_AUDIO\n    KEY_VIDEO\n    KEY_DIRECTORY\n" +
		"    KEY_LIST\n    KEY_MEMO\n    KEY_CALENDAR\n    KEY_RED\n    KEY_GREEN\n    KEY_YELLOW\n    KEY_BLUE\n" +
		"    KEY_CHANNELUP\n    KEY_CHANNELDOWN\n    KEY_FIRST\n    KEY_LAST\n    KEY_AB\n    KEY_NEXT\n" +
		"    KEY_RESTART\n    KEY_SLOW\n    KEY_SHUFFLE\n    KEY_BREAK\n    KEY_PREVIOUS\n    KEY_DIGITS\n" +
		"    KEY_TEEN\n    KEY_TWEN\n    KEY_DEL_EOL\n    KEY_DEL_EOS\n    KEY_INS_LINE\n    KEY_DEL_LINE\n" +
		"    KEY_FN\n    KEY_FN_ESC\n    KEY_FN_F1\n    KEY_FN_F2\n    KEY_FN_F3\n    KEY_FN_F4\n    KEY_FN_F5\n" +
		"    KEY_FN_F6\n    KEY_FN_F7\n    KEY_FN_F8\n    KEY_FN_F9\n    KEY_FN_F10\n    KEY_FN_F11\n" +
		"    KEY_FN_F12\n    KEY_FN_1\n    KEY_FN_2\n    KEY_FN_D\n    KEY_FN_E\n    KEY_FN_F\n    KEY_FN_S\n" +
		"    KEY_FN_B\n    KEY_BRL_DOT1\n    KEY_BRL_DOT2\n    KEY_BRL_DOT3\n    KEY_BRL_DOT4\n    KEY_BRL_DOT5\n" +
		"    KEY_BRL_DOT6\n    KEY_BRL_DOT7\n    KEY_BRL_DOT8\n    BTN_MISC\n    BTN_0\n    BTN_1\n    BTN_2\n" +
		"    BTN_3\n    BTN_4\n    BTN_5\n    BTN_6\n    BTN_7\n    BTN_8\n    BTN_9\n    BTN_MOUSE\n    BTN_LEFT\n" +
		"    BTN_RIGHT\n    BTN_MIDDLE\n    BTN_SIDE\n    BTN_EXTRA\n    BTN_FORWARD\n    BTN_BACK\n    BTN_TASK\n" +
		"    BTN_JOYSTICK\n    BTN_TRIGGER\n    BTN_THUMB\n    BTN_THUMB2\n    BTN_TOP\n    BTN_TOP2\n" +
		"    BTN_PINKIE\n    BTN_BASE\n    BTN_BASE2\n    BTN_BASE3\n    BTN_BASE4\n    BTN_BASE5\n    BTN_BASE6\n" +
		"    BTN_DEAD\n    BTN_GAMEPAD\n    BTN_A\n    BTN_B\n    BTN_C\n    BTN_X\n    BTN_Y\n    BTN_Z\n" +
		"    BTN_TL\n    BTN_TR\n    BTN_TL2\n    BTN_TR2\n    BTN_SELECT\n    BTN_START\n    BTN_MODE\n" +
		"    BTN_THUMBL\n    BTN_THUMBR\n    BTN_DIGI\n    BTN_TOOL_PEN\n    BTN_TOOL_RUBBER\n    BTN_TOOL_BRUSH\n" +
		"    BTN_TOOL_PENCIL\n    BTN_TOOL_AIRBRUSH\n    BTN_TOOL_FINGER\n    BTN_TOOL_MOUSE\n    BTN_TOOL_LENS\n" +
		"    BTN_TOUCH\n    BTN_STYLUS\n    BTN_STYLUS2\n    BTN_TOOL_DOUBLETAP\n    BTN_TOOL_TRIPLETAP\n" +
		"    BTN_WHEEL\n    BTN_GEAR_DOWN\n    BTN_GEAR_UP\r\n"