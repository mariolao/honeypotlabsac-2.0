package br.com.mario.honeypotlabsac.protocol.sms

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.telephony.SmsMessage
import android.util.Log

import java.util.Calendar

import br.mario.labsac.honeypotframework.tools.LogSaver

private const val SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED"

/**
 * BroadcastReceiver for sms. It has the purpose to analyse the received SMSs
 *
 * @author Mário Henrique
 * Created on 02/09/2018.
 * source: https://stackoverflow.com/a/29836639/3443949
 */
class SMSReceiver : BroadcastReceiver() {
	var isRegistered: Boolean = false

	override fun onReceive(context: Context, intent: Intent) {
		val extras = intent.extras

		Log.i("SMS TEST", "Message recieved")

		if (extras != null) {
			val pdus = extras.get("pdus") as Array<*>
			val messages = arrayOfNulls<SmsMessage>(pdus.size)
			for (i in pdus.indices)
				messages[i] = SmsMessage.createFromPdu(pdus[i] as ByteArray)

			if (messages.isNotEmpty()) {
				Log.i("SMS TEST", "Message recieved: " + messages[0]!!.messageBody)
				val adress = messages[0]!!.originatingAddress
				val body = messages[0]!!.messageBody
				val date = messages[0]!!.timestampMillis

				val calendarTime = Calendar.getInstance()
				calendarTime.timeInMillis = date

				val dateTime = String.format("%1\$tF-%1\$tT", calendarTime)
				val string = String.format("%s %s %s", dateTime, adress, body)

				LogSaver("smsLog", string)

				Log.i("SMS TEST", adress)
				Log.i("SMS TEST", dateTime)
				Log.i("SMS TEST", body)
			}
		}
	}

	/**
	 * Register receiver
	 *
	 * @param ctx    - Context
	 * @return see Context.registerReceiver(BroadcastReceiver,IntentFilter)
	 */
	fun register(ctx: Context): Intent? {
		try {
			return if (!isRegistered)
				ctx.registerReceiver(this, IntentFilter(SMS_RECEIVED))
			else
				null

		} finally {
			isRegistered = true
		}
	}

	/**
	 * Unregister received
	 *
	 * @param context - context
	 * @return true if was registered else false
	 */
	fun unregister(context: Context): Boolean {
		// additional work match on context before unregister
		// eg store weak ref in register then compare in unregister
		// if match same instance
		return isRegistered && unregisterInternal(context)
	}

	private fun unregisterInternal(context: Context): Boolean {
		context.unregisterReceiver(this)
		isRegistered = false
		return true
	}
}