package br.com.mario.honeypotlabsac.protocol.telnet

import android.content.Intent

import br.com.mario.honeypotlabsac.R

import br.mario.labsac.honeypotframework.getInt
import br.mario.labsac.honeypotframework.protocol.HoneypotProtocol
import br.mario.labsac.honeypotframework.serviceBasis.GeneralService
import br.mario.labsac.honeypotframework.tools.TRACER.LOGTYPE.INFO

/**
 * Class to build the Telnet service to the application
 *
 * @author Mário Henrique
 * Created  on 11/03/2018. */
class TelnetService : GeneralService(HoneypotProtocol(::TelnetResponse)) {
	override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
		val name = getString(R.string.nameTelnet)
		super.msg.log(INFO, "onStartCommand: $name")

		this.notificationId = getInt(R.integer.portTelnet)
		this.notificationTitle = name
		this.notificationIcon = R.drawable.icon
		// opcional
		this.notificationTicker = "Telnet"

		return (super.onStartCommand(intent, flags, startId))
	}
}