package br.com.mario.honeypotlabsac

import android.app.AlarmManager

import br.com.mario.honeypotlabsac.protocol.telnet.TelnetService

import br.mario.labsac.honeypotframework.KEY_IPSERVER
import br.mario.labsac.honeypotframework.controller.ActivityTranslucent
import br.mario.labsac.honeypotframework.controller.HoneypotManager
import br.mario.labsac.honeypotframework.serviceBasis.ServiceImage

/** Created by MarioH on 29/07/2018. */
class TranslucentActivity : ActivityTranslucent() {
	override var timeInMilliSeconds: Long = AlarmManager.INTERVAL_FIFTEEN_MINUTES

	private lateinit var imageTelnet: ServiceImage

	private var portTelnet = 0
	private var portTelnetAd = 0
	private var serviceTelnet = false

	override fun startComponents() {
		this.portTelnet = honeyApp.getIntPreference("portTelnet", R.integer.portTelnet)
		this.portTelnetAd = honeyApp.getIntPreference("portTelnetAd", R.integer.portTelnetAd)

		// set values
		honeyApp.editor.putString(KEY_IPSERVER, "192.168.1.113").commit()
	}

	/** Inicializa a imagem do serviço Telnet */
	override fun startServiceImages() {
		imageTelnet = ServiceImage(portTelnet, portTelnetAd, getString(R.string.nameTelnet), TelnetService::class.java)

	}

	override fun startImages() {
		if (!serviceTelnet)
			start(true, true, imageTelnet)
		toaster("Telnet")
	}

	/** Update and verify the services are running  */
	override fun updateServices() {
		serviceTelnet = HoneypotManager.checkPort(portTelnetAd)
	}
}