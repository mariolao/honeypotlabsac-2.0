package br.com.mario.honeypotlabsac

import android.Manifest.permission
import android.annotation.TargetApi

import android.os.Build.VERSION_CODES
import android.os.Bundle

import br.com.mario.honeypotlabsac.protocol.sms.SMSReceiver
import br.com.mario.honeypotlabsac.protocol.telnet.TelnetService

import br.mario.labsac.honeypotframework.appHoney
import br.mario.labsac.honeypotframework.controller.ActivityOpaque
import br.mario.labsac.honeypotframework.controller.HoneypotApplication
import br.mario.labsac.honeypotframework.controller.HoneypotManager
import br.mario.labsac.honeypotframework.serviceBasis.ServiceImage

import com.anthonycr.grant.PermissionsManager
import com.crashlytics.android.Crashlytics

import io.fabric.sdk.android.Fabric

import kotlinx.android.synthetic.main.activity_initial.serv01
import kotlinx.android.synthetic.main.activity_initial.servSms
import kotlinx.android.synthetic.main.service_item.view.btStart
import kotlinx.android.synthetic.main.service_item.view.btStop
import kotlinx.android.synthetic.main.service_item.view.tvTitle

import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

//const val RC_PERM_SMS = 125

class LabsacActivity : ActivityOpaque(), AnkoLogger {
	override val updater: (() -> Unit) = {
			info { "telnet: $serviceTelnet | pressed: $pressed "}

			serv01.btStart.isEnabled = (serviceTelnet).not()
			serv01.btStop.isEnabled = (serviceTelnet)

			// TODO adicionar os teste para registro do SMS

			info { "layout configured -->" }
		}
	private var portTelnet = 0
	private var portTelnetAd = 0
	private var serviceTelnet = false
	private var pressed = false
	private var smsPress = false

   private lateinit var imageTelnet: ServiceImage

   override fun onCreate(savedInstanceState: Bundle?) {
	   super.onCreate(savedInstanceState)

	   serv01.tvTitle.setText(R.string.nameTelnet)

	   serv01.btStart.setOnClickListener {
		   pressed = true
		   start(true, true, imageTelnet)
		   updateLayout(1000)
	   }

	   serv01.btStop.setOnClickListener {
		   pressed = false
		   stop(true, true, imageTelnet)
		   updateLayout()
	   }

//	   val smsReceiver: SMSReceiver =
//	   if (appHoney.mapOfThings.containsKey("sms")) {
//		   appHoney.mapOfThings["sms"] as SMSReceiver
//	   } else {
//		   SMSReceiver().apply { appHoney.mapOfThings.put("sms", this) }
//	   }

	   val smsReceiver: SMSReceiver =
		   appHoney.mapOfThings["sms"] as SMSReceiver? ?:
		   SMSReceiver().apply { appHoney.mapOfThings["sms"] = this }

	   servSms.apply {
		   tvTitle.text = "SMS Service"

		   btStart.setOnClickListener {
			   smsPress = true
			   // start
			   smsReceiver.register(this@LabsacActivity)
			   updateLayout()
		   }

		   btStop.setOnClickListener {
			   smsPress = false
			   // stop
			   smsReceiver.unregister(this@LabsacActivity)
			   updateLayout()
		   }
	   }


	   Fabric.with(this, Crashlytics())
//	   CommonUtils.isRooted(this)

	   managePermissions()
   }

	private fun managePermissions() {
		val wantedPerm = arrayOf(permission.READ_SMS, permission.RECEIVE_SMS, permission.READ_CALL_LOG, permission.WRITE_CALL_LOG)
		//	   if (EasyPermissions.hasPermissions(this, *wantedPerm)) {}
		//	   else {
		//		   EasyPermissions.requestPermissions(this, "This app needs access to send SMS.", RC_PERM_SMS, *wantedPerm)
		//	   }

		PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult(this, wantedPerm, null)

		/*val permRead = Manifest.permission.READ_SMS
		val permRec = Manifest.permission.RECEIVE_SMS
		if (ContextCompat.checkSelfPermission(baseContext, permRead) != PackageManager.PERMISSION_GRANTED) {
//		   permissionList.add(permission);
			if (ActivityCompat.shouldShowRequestPermissionRationale(this, permRead)) { }
			else
				ActivityCompat.requestPermissions(this, arrayOf(permRead), 1)
		}*/
	}

	@TargetApi(VERSION_CODES.M)
	override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
		PermissionsManager.getInstance().notifyPermissionsChange(permissions, grantResults)
	}

   /** Inicioliza as informaações das portas para o serviço Telnet*/
   override fun startComponents() {
	   val app = application as HoneypotApplication

      this.portTelnet = app.getIntPreference("portTelnet", R.integer.portTelnet)
      this.portTelnetAd = app.getIntPreference("portTelnetAd", R.integer.portTelnetAd)

//	   app.editor.putString(KEY_IPSERVER, "192.168.1.113").commit()
//	   app.editor.putLong(KEY_TIMEALARM, 0L)
   }

   /** Inicializa a imagem do serviço Telnet */
   override fun startServiceImages() {
	   imageTelnet = ServiceImage(portTelnet, portTelnetAd, getString(R.string.nameTelnet), TelnetService::class.java)
   }

   override fun updateServices() {
	   serviceTelnet = HoneypotManager.checkPort(portTelnetAd)
   }

	/*override fun updateLayout(time: Long, function: (() -> Unit)?) {
		info { "<-- configuring layout" }
		super.updateLayout(time) {
			info { "telnet: $serviceTelnet | pressed: $pressed "}

			serv01.btStart.isEnabled = (serviceTelnet).not()
			serv01.btStop.isEnabled = (serviceTelnet)

			info { "layout configured -->" }
		}
	}*/
}