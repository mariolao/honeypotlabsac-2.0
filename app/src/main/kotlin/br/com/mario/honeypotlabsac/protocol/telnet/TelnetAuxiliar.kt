//@file:JvmName("TelnetUtils")
package br.com.mario.honeypotlabsac.protocol.telnet

import android.Manifest.permission

import android.content.ClipboardManager
import android.content.Context
import android.content.ContextWrapper
import android.content.pm.PackageManager.PERMISSION_DENIED

import android.hardware.Sensor
import android.hardware.Sensor.TYPE_ACCELEROMETER
import android.hardware.Sensor.TYPE_AMBIENT_TEMPERATURE
import android.hardware.Sensor.TYPE_GYROSCOPE
import android.hardware.Sensor.TYPE_LIGHT
import android.hardware.Sensor.TYPE_MAGNETIC_FIELD
import android.hardware.Sensor.TYPE_PRESSURE
import android.hardware.Sensor.TYPE_PROXIMITY
import android.hardware.Sensor.TYPE_RELATIVE_HUMIDITY
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.hardware.SensorManager.SENSOR_DELAY_NORMAL

import android.net.ConnectivityManager
import android.net.wifi.WifiManager

import android.os.Build.VERSION
import android.os.Build.VERSION_CODES

import android.provider.Settings.System

import android.telephony.TelephonyManager

import android.util.Log

import br.mario.labsac.honeypotframework.app

/**
 * This file contains classes and propeties that help on management of Sensors
 *
 * @author Mário Henrique
 * Created on 18/07/2018.
 */
val sensorManager: SensorManager?
	get() {
		return (app.getSystemService(Context.SENSOR_SERVICE) as SensorManager)
	}

val clipboard: ClipboardManager?
	get() = app.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?

val wifiManager: WifiManager
	get() = app.getSystemService(Context.WIFI_SERVICE) as WifiManager

val dataManager: ConnectivityManager
	get() = app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

val telManager: TelephonyManager
	get() = app.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

fun setDataEnabled(enable: Boolean) {
	enforceModifyPermission(app, permission.MODIFY_PHONE_STATE)
	try {
		telManager.javaClass.getDeclaredMethod("setDataEnabled", Boolean::class.java)?.
			invoke(telManager, enable)

	} catch (ex: Exception) {
		Log.e("DATA_MANAGER", "Error setting mobile data state", ex)
	}
}

fun setAutorotate(enable: Boolean) {
	enforceModifyPermission(app, permission.WRITE_SETTINGS)
	System.putInt(app.contentResolver, System.ACCELEROMETER_ROTATION, if (enable) 1 else 0)
}

private fun enforceModifyPermission(ctx: Context, permission: String) {
	val wrapper = ContextWrapper(ctx)
	if (wrapper.checkCallingOrSelfPermission(permission) == PERMISSION_DENIED)
		wrapper.enforceCallingOrSelfPermission(permission, null)
}

object Sensors {
	private lateinit var acceSensor: AccelerometerSensor
	private lateinit var proxSensor: ProximitySensor
	private lateinit var lghtSensor: LightSensor
	private lateinit var magnSensor: MagneticFieldSensor
	private lateinit var tempSensor: TemperatureSensor
	private lateinit var presSensor: PressureSensor
	private lateinit var gyroSensor: GyroscopeSensor
	private lateinit var humiSensor: HumiditySensor

	val proxStatus
		get() = proxSensor.sensorStatus
	val gyroStatus
		get() = gyroSensor.sensorStatus

	val accel
		get() = acceSensor.value
	val distance
		get() = proxSensor.value
	val gyro
		get() = gyroSensor.value
	val lux
		get() = lghtSensor.value
	val pressure
		get() = presSensor.value
	val temp
		get() = tempSensor.value
	val humi
		get() = humiSensor.value
	val magn
		get() = magnSensor.value
	val orient: String
		get() {
			// Rotation matrix based on current readings from accelerometer and magnetometer.
			val rotationMatrix = FloatArray(9)
			SensorManager.getRotationMatrix(rotationMatrix, null, acceSensor.accel, magnSensor.magnetic)
			// Express the updated rotation matrix as three orientation angles.
			val orientationAngles = FloatArray(3)
			SensorManager.getOrientation(rotationMatrix, orientationAngles)

			return (orientationAngles.zip("XYZ".toList()).fold("") { acc, pair ->
				"$acc${pair.second}: ${pair.first} "
			})
		}

	fun startListeners() {
		acceSensor = AccelerometerSensor()
		gyroSensor = GyroscopeSensor()
		proxSensor = ProximitySensor()
		lghtSensor = LightSensor()
		magnSensor = MagneticFieldSensor()
		humiSensor = HumiditySensor()
	}

	fun stopListeners() {
		acceSensor.stopListener()
		gyroSensor.stopListener()
		proxSensor.stopListener()
		lghtSensor.stopListener()
		magnSensor.stopListener()
		humiSensor.stopListener()
	}
}

abstract class SensorAdapter(var value: String, type: Int) : SensorEventListener {
	private val sensor: Sensor? by lazy { sensorManager?.getDefaultSensor(type) }
	internal val sensorStatus
		get() = sensor != null
	internal val maximumRange
		get() = sensor?.maximumRange

	init { startListener() }

	private fun startListener() {
		if (VERSION.SDK_INT >= VERSION_CODES.KITKAT)
			sensorManager?.registerListener(this, sensor, SENSOR_DELAY_NORMAL,
				SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM)
		else
			sensorManager?.registerListener(this, sensor, SENSOR_DELAY_NORMAL)
	}

	internal fun stopListener() {
		sensorManager?.unregisterListener(this)
	}

	/**
	 * Called when the accuracy of the registered sensor has changed.  Unlike
	 * onSensorChanged(), this is only called when this accuracy value changes.
	 *
	 *
	 * See the SENSOR_STATUS_* constants in
	 * [SensorManager][android.hardware.SensorManager] for details.
	 *
	 * @param accuracy The new accuracy of this sensor, one of
	 * `SensorManager.SENSOR_STATUS_*`
	 */
	final override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

	/**
	 * Called when there is a new sensor event.  Note that "on changed"
	 * is somewhat of a misnomer, as this will also be called if we have a
	 * new reading from a sensor with the exact same sensor values (but a
	 * newer timestamp).
	 *
	 *
	 * See [SensorManager][android.hardware.SensorManager]
	 * for details on possible sensor types.
	 *
	 * See also [SensorEvent][android.hardware.SensorEvent].
	 *
	 *
	 * **NOTE:** The application doesn't own the
	 * [event][android.hardware.SensorEvent]
	 * object passed as a parameter and therefore cannot hold on to it.
	 * The object may be part of an internal pool and may be reused by
	 * the framework.
	 *
	 * @param event the [SensorEvent][android.hardware.SensorEvent].
	 */
	abstract override fun onSensorChanged(event: SensorEvent)
}

class AccelerometerSensor(var accel: FloatArray = floatArrayOf())
	: SensorAdapter("X: 0 Y: 0 Z: 0 m/s^2", TYPE_ACCELEROMETER) {
	override fun onSensorChanged(event: SensorEvent) {
		accel = event.values
		value = event.values.zip("XYZ".toList()).fold("") { acc, pair ->
			"$acc${pair.second}: ${pair.first} "
		}
		value += "m/s^2"
	}
}

class GyroscopeSensor : SensorAdapter("X: 0 Y: -0 Z: 0 rad", TYPE_GYROSCOPE) {
	override fun onSensorChanged(event: SensorEvent) {
		value = event.values.zip("XYZ".toList()).fold("") { acc, pair ->
			"$acc${pair.second}: ${pair.first} "
		}
		value += " rad"
	}
}

class HumiditySensor: SensorAdapter("0 %", TYPE_RELATIVE_HUMIDITY) {
	override fun onSensorChanged(event: SensorEvent) {
		value = "${event.values[0]} %"
	}
}

class LightSensor : SensorAdapter("0.0 lux", TYPE_LIGHT) {
	override fun onSensorChanged(event: SensorEvent) {
		value = ("${event.values[0]} lux")
	}
}

class MagneticFieldSensor(var magnetic: FloatArray = floatArrayOf())
	: SensorAdapter("X: 0 Y: -0 Z: 0 uT", TYPE_MAGNETIC_FIELD) {
	override fun onSensorChanged(event: SensorEvent) {
		magnetic = event.values
		value = "${event.values[0]} uT"
	}
}

class PressureSensor : SensorAdapter("0 mbar", TYPE_PRESSURE) {
	override fun onSensorChanged(event: SensorEvent) {
		value = "${event.values[0]} mbar"
	}
}

class ProximitySensor : SensorAdapter("far", TYPE_PROXIMITY) {
	override fun onSensorChanged(event: SensorEvent) {
		value = if (event.values[0] >= maximumRange!!)
			"far" // >= 5cm
		else
			"near" // < 5cm
	}
}

class TemperatureSensor : SensorAdapter("0 ºC", TYPE_AMBIENT_TEMPERATURE) {
	override fun onSensorChanged(event: SensorEvent) {
		value = "${event.values[0]} ºC"
	}
}