package br.com.mario.honeypotlabsac.protocol.telnet.util

/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.annotation.TargetApi
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Build.VERSION_CODES
import android.os.Environment
import android.provider.BaseColumns
import android.telephony.SmsMessage
import android.telephony.SubscriptionManager
import android.text.TextUtils
import android.util.Config
import android.util.Patterns
import br.com.mario.honeypotlabsac.protocol.telnet.util.SdkConstant.SdkConstantType
import java.util.regex.Pattern

/**
 * The Telephony provider contains data related to phone operation.
 *
 * @hide
 */
object Telephony {
	//	private static final String TAG = "Telephony";
	private val DEBUG = true
	private val LOCAL_LOGV = if (DEBUG) Config.LOGD else Config.LOGV


	/** Base columns for tables that contain text based SMSs. */
	interface TextBasedSmsColumns {

		companion object {
			/**
			 * The type of the message
			 *
			 * Type: INTEGER
			 */
			val TYPE = "type"

			val MESSAGE_TYPE_ALL = 0
			val MESSAGE_TYPE_INBOX = 1
			val MESSAGE_TYPE_SENT = 2
			val MESSAGE_TYPE_DRAFT = 3
			val MESSAGE_TYPE_OUTBOX = 4
			val MESSAGE_TYPE_FAILED = 5 // for failed outgoing messages
			val MESSAGE_TYPE_QUEUED = 6 // for messages to send later

			/**
			 * The thread ID of the message
			 *
			 * Type: INTEGER
			 */
			const val THREAD_ID = "thread_id"

			/**
			 * The address of the other party
			 *
			 * Type: TEXT
			 */
			const val ADDRESS = "address"

			/**
			 * The person ID of the sender
			 * <P>
			 * Type: INTEGER (long)
			</P> *
			 */
			const val PERSON_ID = "person"

			/**
			 * The date the message was sent
			 * <P>
			 * Type: INTEGER (long)
			</P> *
			 */
			val DATE = "date"

			/**
			 * Has the message been read
			 * <P>
			 * Type: INTEGER (boolean)
			</P> *
			 */
			val READ = "read"

			/**
			 * Indicates whether this message has been seen by the user. The "seen" flag will be used to
			 * figure out whether we need to throw up a statusbar notification or not.
			 */
			val SEEN = "seen"

			/**
			 * The TP-Status value for the message, or -1 if no status has been received
			 */
			val STATUS = "status"

			val STATUS_NONE = -1
			val STATUS_COMPLETE = 0
			val STATUS_PENDING = 32
			val STATUS_FAILED = 64

			/**
			 * The subject of the message, if present
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val SUBJECT = "subject"

			/**
			 * The body of the message
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val BODY = "body"

			/**
			 * The id of the sender of the conversation, if present
			 * <P>
			 * Type: INTEGER (reference to item in content://contacts/people)
			</P> *
			 */
			val PERSON = "person"

			/**
			 * The protocol identifier code
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val PROTOCOL = "protocol"

			/**
			 * Whether the `TP-Reply-Path` bit was set on this message
			 * <P>
			 * Type: BOOLEAN
			</P> *
			 */
			val REPLY_PATH_PRESENT = "reply_path_present"

			/**
			 * The service center (SC) through which to send the message, if present
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val SERVICE_CENTER = "service_center"

			/**
			 * Has the message been locked?
			 *
			 * Type: INTEGER (boolean)
			 */
			val LOCKED = "locked"

			/**
			 * Error code associated with sending or receiving this message
			 *
			 * Type: INTEGER
			 */
			val ERROR_CODE = "error_code"

			/**
			 * Meta data used externally.
			 *
			 * Type: TEXT
			 */
			val META_DATA = "meta_data"

			val LIMIT_TICKER = 71
		}
	}

	/** Contains all text based SMS messages. */
	class Sms private constructor(): BaseColumns,
		TextBasedSmsColumns {
		/** Contains all text based SMS messages in the SMS app's inbox. */
		class Inbox : BaseColumns, TextBasedSmsColumns {
			companion object {
				/** The content:// style URL for this table */
				val CONTENT_URI = Uri.parse("content://sms/inbox")

				/** The default sort order for this table */
				val DEFAULT_SORT_ORDER = "date DESC"

				/**
				 * Add an SMS to the Draft box.
				 *
				 * @param resolver
				 * the content resolver to use
				 * @param address
				 * the address of the sender
				 * @param body
				 * the body of the message
				 * @param subject
				 * the psuedo-subject of the message
				 * @param date
				 * the timestamp for the message
				 * @param read
				 * true if the message has been read, false if not
				 * @return the URI for the new message
				 */
				fun addMessage(resolver: ContentResolver,
				               address: String, body: String, subject: String?, date: Long?,
				               read: Boolean) : Uri? {
					return addMessageToUri(resolver, CONTENT_URI, address, body,
						subject, date, read, false)
				}

				@TargetApi(VERSION_CODES.N)
				fun addMessage(subId: Int = SubscriptionManager.getDefaultSmsSubscriptionId(), resolver: ContentResolver,
				               address: String, body: String, subject: String?, date: Long?,
				               read: Boolean): Uri? {
					return addMessageToUri(subId,
						resolver, CONTENT_URI, address, body, subject, date, read, false)
				}
			}
		}

		/** Contains all sent text based SMS messages in the SMS app's. */
		class Sent : BaseColumns, TextBasedSmsColumns {
			companion object {
				/**
				 * The content:// style URL for this table
				 */
				val CONTENT_URI = Uri.parse("content://sms/sent")

				/**
				 * The default sort order for this table
				 */
				val DEFAULT_SORT_ORDER = "date DESC"

				/**
				 * Add an SMS to the Draft box.
				 *
				 * @param resolver
				 * the content resolver to use
				 * @param address
				 * the address of the sender
				 * @param body
				 * the body of the message
				 * @param subject
				 * the psuedo-subject of the message
				 * @param date
				 * the timestamp for the message
				 * @return the URI for the new message
				 */
				fun addMessage(resolver: ContentResolver,
				               address: String, body: String, subject: String, date: Long?): Uri? {
					return addMessageToUri(resolver, CONTENT_URI, address, body,
						subject, date, true, false)
				}
			}
		}

		/** Contains all sent text based SMS messages in the SMS app's. */
		class Draft : BaseColumns, TextBasedSmsColumns {
			companion object {
				/**
				 * The content:// style URL for this table
				 */
				val CONTENT_URI = Uri.parse("content://sms/draft")

				/**
				 * The default sort order for this table
				 */
				val DEFAULT_SORT_ORDER = "date DESC"

				/**
				 * Add an SMS to the Draft box.
				 *
				 * @param resolver
				 * the content resolver to use
				 * @param address
				 * the address of the sender
				 * @param body
				 * the body of the message
				 * @param subject
				 * the psuedo-subject of the message
				 * @param date
				 * the timestamp for the message
				 * @return the URI for the new message
				 */
				fun addMessage(resolver: ContentResolver,
				               address: String, body: String, subject: String, date: Long?): Uri? {
					return addMessageToUri(resolver, CONTENT_URI, address, body,
						subject, date, true, false)
				}

				/**
				 * Save over an existing draft message.
				 *
				 * @param resolver
				 * the content resolver to use
				 * @param uri
				 * of existing message
				 * @param body
				 * the new body for the draft message
				 * @return true is successful, false otherwise
				 */
				fun saveMessage(resolver: ContentResolver,
				                uri: Uri, body: String): Boolean {
					val values = ContentValues(2)
					values.put(TextBasedSmsColumns.BODY, body)
					values.put(TextBasedSmsColumns.DATE, System.currentTimeMillis())
					return resolver.update(uri, values, null, null) == 1
				}
			}
		}

		/** Contains all pending outgoing text based SMS messages. */
		class Outbox : BaseColumns, TextBasedSmsColumns {
			companion object {
				/**
				 * The content:// style URL for this table
				 */
				val CONTENT_URI = Uri.parse("content://sms/outbox")

				/**
				 * The default sort order for this table
				 */
				val DEFAULT_SORT_ORDER = "date DESC"

				/**
				 * Add an SMS to the Out box.
				 *
				 * @param resolver
				 * the content resolver to use
				 * @param address
				 * the address of the sender
				 * @param body
				 * the body of the message
				 * @param subject
				 * the psuedo-subject of the message
				 * @param date
				 * the timestamp for the message
				 * @param deliveryReport
				 * whether a delivery report was requested for the message
				 * @return the URI for the new message
				 */
				fun addMessage(resolver: ContentResolver,
				               address: String, body: String, subject: String, date: Long?,
				               deliveryReport: Boolean, threadId: Long): Uri? {
					return addMessageToUri(resolver, CONTENT_URI, address, body,
						subject, date, true, deliveryReport, threadId)
				}
			}
		}

		/** Contains all sent text-based SMS messages in the SMS app's. */
		class Conversations : BaseColumns,
			TextBasedSmsColumns {
			companion object {
				/**
				 * The content:// style URL for this table
				 */
				val CONTENT_URI = Uri.parse("content://sms/conversations")

				/**
				 * The default sort order for this table
				 */
				val DEFAULT_SORT_ORDER = "date DESC"

				/**
				 * The first 45 characters of the body of the message
				 * <P>
				 * Type: TEXT
				</P> *
				 */
				val SNIPPET = "snippet"

				/**
				 * The number of messages in the conversation
				 * <P>
				 * Type: INTEGER
				</P> *
				 */
				val MESSAGE_COUNT = "msg_count"
			}
		}

		/** Contains info about SMS related Intents that are broadcast. */
		object Intents {
			/**
			 * Set by BroadcastReceiver. Indicates the message was handled successfully.
			 */
			val RESULT_SMS_HANDLED = 1

			/**
			 * Set by BroadcastReceiver. Indicates a generic error while processing the message.
			 */
			val RESULT_SMS_GENERIC_ERROR = 2

			/**
			 * Set by BroadcastReceiver. Indicates insufficient memory to store the message.
			 */
			val RESULT_SMS_OUT_OF_MEMORY = 3

			/**
			 * Set by BroadcastReceiver. Indicates the message, while possibly valid, is of a format or
			 * encoding that is not supported.
			 */
			val RESULT_SMS_UNSUPPORTED = 4

			/**
			 * Broadcast Action: A new text based SMS message has been received by the device. The
			 * intent will have the following extra values:
			 *
			 *
			 *  * *pdus* - An Object[] od byte[]s containing the PDUs that make up the message.
			 *
			 *
			 *
			 *
			 *
			 * The extra values can be extracted using [.getMessagesFromIntent].
			 *
			 *
			 *
			 *
			 * If a BroadcastReceiver encounters an error while processing this intent it should set
			 * the result code appropriately.
			 *
			 */
			@SdkConstant(SdkConstantType.BROADCAST_INTENT_ACTION)
			val SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED"

			/**
			 * Broadcast Action: A new data based SMS message has been received by the device. The
			 * intent will have the following extra values:
			 *
			 *
			 *  * *pdus* - An Object[] of byte[]s containing the PDUs that make up the message.
			 *
			 *
			 *
			 *
			 *
			 * The extra values can be extracted using [.getMessagesFromIntent].
			 *
			 *
			 *
			 *
			 * If a BroadcastReceiver encounters an error while processing this intent it should set
			 * the result code appropriately.
			 *
			 */
			@SdkConstant(SdkConstantType.BROADCAST_INTENT_ACTION)
			val DATA_SMS_RECEIVED_ACTION = "android.intent.action.DATA_SMS_RECEIVED"

			/**
			 * Broadcast Action: A new WAP PUSH message has been received by the device. The intent
			 * will have the following extra values:
			 *
			 *
			 *  * *transactionId (Integer)* - The WAP transaction ID
			 *  * *pduType (Integer)* - The WAP PDU type
			 *  * *header (byte[])* - The header of the message
			 *  * *data (byte[])* - The data payload of the message
			 *  * *contentTypeParameters (HashMap&lt;String,String&gt;)* - Any parameters
			 * associated with the content type (decoded from the WSP Content-Type header)
			 *
			 *
			 *
			 *
			 * If a BroadcastReceiver encounters an error while processing this intent it should set
			 * the result code appropriately.
			 *
			 *
			 *
			 *
			 * The contentTypeParameters extra value is map of content parameters keyed by their names.
			 *
			 *
			 *
			 *
			 * If any unassigned well-known parameters are encountered, the key of the map will be
			 * 'unassigned/0x...', where '...' is the hex value of the unassigned parameter. If a
			 * parameter has No-Value the value in the map will be null.
			 *
			 */
			@SdkConstant(SdkConstantType.BROADCAST_INTENT_ACTION)
			val WAP_PUSH_RECEIVED_ACTION = "android.provider.Telephony.WAP_PUSH_RECEIVED"

			/**
			 * Broadcast Action: A new Cell Broadcast message has been received by the device. The
			 * intent will have the following extra values:
			 *
			 *
			 *  * *pdus* - An Object[] of byte[]s containing the PDUs that make up the message.
			 *
			 *
			 *
			 *
			 *
			 * The extra values can be extracted using [.getMessagesFromIntent].
			 *
			 *
			 *
			 *
			 * If a BroadcastReceiver encounters an error while processing this intent it should set
			 * the result code appropriately.
			 *
			 */
			@SdkConstant(SdkConstantType.BROADCAST_INTENT_ACTION)
			val SMS_CB_RECEIVED_ACTION = "android.provider.Telephony.SMS_CB_RECEIVED"

			/**
			 * Broadcast Action: A new Emergency Broadcast message has been received by the device. The
			 * intent will have the following extra values:
			 *
			 *
			 *  * *pdus* - An Object[] of byte[]s containing the PDUs that make up the message.
			 *
			 *
			 *
			 *
			 *
			 * The extra values can be extracted using [.getMessagesFromIntent].
			 *
			 *
			 *
			 *
			 * If a BroadcastReceiver encounters an error while processing this intent it should set
			 * the result code appropriately.
			 *
			 */
			@SdkConstant(SdkConstantType.BROADCAST_INTENT_ACTION)
			val SMS_EMERGENCY_CB_RECEIVED_ACTION = "android.provider.Telephony.SMS_EMERGENCY_CB_RECEIVED"

			/**
			 * Broadcast Action: The SIM storage for SMS messages is full. If space is not freed,
			 * messages targeted for the SIM (class 2) may not be saved.
			 */
			@SdkConstant(SdkConstantType.BROADCAST_INTENT_ACTION)
			val SIM_FULL_ACTION = "android.provider.Telephony.SIM_FULL"

			/**
			 * Broadcast Action: An incoming SMS has been rejected by the telephony framework. This
			 * intent is sent in lieu of any of the RECEIVED_ACTION intents. The intent will have the
			 * following extra value:
			 *
			 *
			 *  * *result* - An int result code, eg,
			 * `[.RESULT_SMS_OUT_OF_MEMORY]`, indicating the error returned to the
			 * network.
			 *
			 */
			@SdkConstant(SdkConstantType.BROADCAST_INTENT_ACTION)
			val SMS_REJECTED_ACTION = "android.provider.Telephony.SMS_REJECTED"

			/**
			 * Read the PDUs out of an [.SMS_RECEIVED_ACTION] or a
			 * [.DATA_SMS_RECEIVED_ACTION] intent.
			 *
			 * @param intent
			 * the intent to read from
			 * @return an array of SmsMessages for the PDUs
			 */
			fun getMessagesFromIntent(
				intent: Intent): Array<SmsMessage?> {
				val messages = intent.getSerializableExtra("pdus") as Array<Any>
				val pduObjs = arrayOfNulls<ByteArray>(messages.size)

				for (i in messages.indices) {
					pduObjs[i] = messages[i] as ByteArray
				}
				val pdus = arrayOfNulls<ByteArray>(pduObjs.size)
				val pduCount = pdus.size
				val msgs = arrayOfNulls<SmsMessage>(pduCount)
				for (i in 0 until pduCount) {
					pdus[i] = pduObjs[i]
					msgs[i] = SmsMessage.createFromPdu(pdus[i])
				}
				return msgs
			}
		}

		companion object {

			fun query(cr: ContentResolver, projection: Array<String>): Cursor? {
				return cr.query(CONTENT_URI, projection, null, null, DEFAULT_SORT_ORDER)
			}

			fun query(cr: ContentResolver, projection: String,
			          where: String): Cursor? {
				return cr.query(CONTENT_URI, arrayOf(projection), where, null, DEFAULT_SORT_ORDER)
			}

			fun query(cr: ContentResolver, projection: Array<String>,
			          where: String, orderBy: String?): Cursor? {
				return cr.query(CONTENT_URI, projection, where, null, orderBy ?: DEFAULT_SORT_ORDER)
			}

			/** The "content://sms" URL for this table  */
			val CONTENT_URI = Uri.parse("content://sms/") // "content://sms"

			/** The "content://mms-sms/conversations" URL for this table  */
			val CONTENT_CONVERSATION = Uri.parse("content://mms-sms/conversations")

			/**
			 * The default sort order for this table
			 */
			val DEFAULT_SORT_ORDER = "date DESC"

		/*	*//**
			 * Add an SMS to the given URI.
			 *
			 * @param resolver
			 * the content resolver to use
			 * @param uri
			 * the URI to add the message to
			 * @param address
			 * the address of the sender
			 * @param body
			 * the body of the message
			 * @param subject
			 * the psuedo-subject of the message
			 * @param date
			 * the timestamp for the message
			 * @param read
			 * true if the message has been read, false if not
			 * @param deliveryReport
			 * true if a delivery report was requested, false if not
			 * @return the URI for the new message
			 *//*
			fun addMessageToUri(resolver: ContentResolver,
			                    uri: Uri, address: String, body: String, subject: String,
			                    date: Long?, read: Boolean, deliveryReport: Boolean): Uri? {
				return addMessageToUri(resolver, uri, address, body, subject,
					date, read, deliveryReport, -1L)
			}*/

			/**
			 * Add an SMS to the given URI with thread_id specified.
			 *
			 * @param resolver
			 * the content resolver to use
			 * @param uri
			 * the URI to add the message to
			 * @param address
			 * the address of the sender
			 * @param body
			 * the body of the message
			 * @param subject
			 * the psuedo-subject of the message. [It can be null]
			 * @param date
			 * the timestamp for the message
			 * @param read
			 * true if the message has been read, false if not
			 * @param deliveryReport
			 * true if a delivery report was requested, false if not
			 * @param threadId
			 * the thread_id of the message
			 * @return the URI for the new message
			 */
			fun addMessageToUri(resolver: ContentResolver,
			                    uri: Uri, address: String, body: String, subject: String?,
			                    date: Long?, read: Boolean, deliveryReport: Boolean, threadId: Long = -1): Uri? {
				val values = ContentValues(7)

				values.put(TextBasedSmsColumns.ADDRESS, address)

				if (date != null) {
					values.put(TextBasedSmsColumns.DATE, date)
				}

				values.put(TextBasedSmsColumns.READ, if (read) Integer.valueOf(1) else Integer.valueOf(0))

				if (subject != null) {
					values.put(TextBasedSmsColumns.SUBJECT, subject)
				}

				values.put(TextBasedSmsColumns.BODY, body)

				if (deliveryReport) {
					values.put(TextBasedSmsColumns.STATUS, TextBasedSmsColumns.STATUS_PENDING)
				}
				if (threadId != -1L) {
					values.put(TextBasedSmsColumns.THREAD_ID, threadId)
				}

				return resolver.insert(uri, values)
			}

			@TargetApi(VERSION_CODES.N)
			fun addMessageToUri(subId: Int, resolver: ContentResolver, uri: Uri, address: String, body: String,
			                    subject: String?, date: Long?, read: Boolean, deliveryReport: Boolean,
			                    threadId: Long = -1): Uri? {
				return (resolver.insert(uri,
					ContentValues(8).apply {
						put(TextBasedSmsColumns.ADDRESS, address)
						if (date != null) put(TextBasedSmsColumns.DATE, date)
						put(TextBasedSmsColumns.READ, if (read) Integer.valueOf(1) else Integer.valueOf(0))
						if (subject != null) put(TextBasedSmsColumns.SUBJECT, subject)
						put(TextBasedSmsColumns.BODY, body)
						if (deliveryReport)
							put(TextBasedSmsColumns.STATUS, TextBasedSmsColumns.STATUS_PENDING)
						if (threadId != -1L)
							put(TextBasedSmsColumns.THREAD_ID, threadId)
					}))
			}

			/**
			 * Move a message to the given folder.
			 *
			 * @param context
			 * the context to use
			 * @param uri
			 * the message to move
			 * @param folder
			 * the folder to move to
			 * @return true if the operation succeeded
			 */
			/*public static boolean moveMessageToFolder(Context context,
				Uri uri, int folder, int error) {
			if (uri == null) {
				return false;
			}

			boolean markAsUnread = false;
			boolean markAsRead = false;

			switch (folder) {
				case MESSAGE_TYPE_INBOX:
				case MESSAGE_TYPE_DRAFT:
					break;
				case MESSAGE_TYPE_OUTBOX:
				case MESSAGE_TYPE_SENT:
					markAsRead = true;
					break;
				case MESSAGE_TYPE_FAILED:
				case MESSAGE_TYPE_QUEUED:
					markAsUnread = true;
					break;
				default:
					return (false);
			}

			ContentValues values = new ContentValues(3);

			values.put(TYPE, folder);
			if (markAsUnread) {
				values.put(READ, Integer.valueOf(0));
			} else if (markAsRead) {
				values.put(READ, Integer.valueOf(1));
			}
			values.put(ERROR_CODE, error);

			return (1 == SqliteWrapper.update(context, context.getContentResolver(),
					uri, values, null, null));
		}*/

			/** Returns true if the folder (message type) identifies an outgoing message.  */
			fun isOutgoingFolder(messageType: Int): Boolean {
				return (messageType == TextBasedSmsColumns.MESSAGE_TYPE_FAILED
					|| messageType == TextBasedSmsColumns.MESSAGE_TYPE_OUTBOX
					|| messageType == TextBasedSmsColumns.MESSAGE_TYPE_SENT
					|| messageType == TextBasedSmsColumns.MESSAGE_TYPE_QUEUED)
			}
		}
	}

	/**
	 * Base columns for tables that contain MMSs.
	 */
	interface BaseMmsColumns : BaseColumns {
		companion object {

			val MESSAGE_BOX_ALL = 0
			val MESSAGE_BOX_INBOX = 1
			val MESSAGE_BOX_SENT = 2
			val MESSAGE_BOX_DRAFTS = 3
			val MESSAGE_BOX_OUTBOX = 4

			/**
			 * The date the message was sent.
			 * <P>
			 * Type: INTEGER (long)
			</P> *
			 */
			val DATE = "date"

			/**
			 * The box which the message belong to, for example, MESSAGE_BOX_INBOX.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val MESSAGE_BOX = "msg_box"

			/**
			 * Has the message been read.
			 * <P>
			 * Type: INTEGER (boolean)
			</P> *
			 */
			val READ = "read"

			/**
			 * Indicates whether this message has been seen by the user. The "seen" flag will be used to
			 * figure out whether we need to throw up a statusbar notification or not.
			 */
			val SEEN = "seen"

			/**
			 * The Message-ID of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val MESSAGE_ID = "m_id"

			/**
			 * The subject of the message, if present.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val SUBJECT = "sub"

			/**
			 * The character set of the subject, if present.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val SUBJECT_CHARSET = "sub_cs"

			/**
			 * The Content-Type of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val CONTENT_TYPE = "ct_t"

			/**
			 * The Content-Location of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val CONTENT_LOCATION = "ct_l"

			/**
			 * The address of the sender.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val FROM = "from"

			/**
			 * The address of the recipients.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val TO = "to"

			/**
			 * The address of the cc. recipients.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val CC = "cc"

			/**
			 * The address of the bcc. recipients.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val BCC = "bcc"

			/**
			 * The expiry time of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val EXPIRY = "exp"

			/**
			 * The class of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val MESSAGE_CLASS = "m_cls"

			/**
			 * The type of the message defined by MMS spec.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val MESSAGE_TYPE = "m_type"

			/**
			 * The version of specification that this message conform.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val MMS_VERSION = "v"

			/**
			 * The size of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val MESSAGE_SIZE = "m_size"

			/**
			 * The priority of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val PRIORITY = "pri"

			/**
			 * The read-report of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val READ_REPORT = "rr"

			/**
			 * Whether the report is allowed.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val REPORT_ALLOWED = "rpt_a"

			/**
			 * The response-status of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val RESPONSE_STATUS = "resp_st"

			/**
			 * The status of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val STATUS = "st"

			/**
			 * The transaction-id of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val TRANSACTION_ID = "tr_id"

			/**
			 * The retrieve-status of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val RETRIEVE_STATUS = "retr_st"

			/**
			 * The retrieve-text of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val RETRIEVE_TEXT = "retr_txt"

			/**
			 * The character set of the retrieve-text.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val RETRIEVE_TEXT_CHARSET = "retr_txt_cs"

			/**
			 * The read-status of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val READ_STATUS = "read_status"

			/**
			 * The content-class of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val CONTENT_CLASS = "ct_cls"

			/**
			 * The delivery-report of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val DELIVERY_REPORT = "d_rpt"

			/**
			 * The delivery-time-token of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val DELIVERY_TIME_TOKEN = "d_tm_tok"

			/**
			 * The delivery-time of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val DELIVERY_TIME = "d_tm"

			/**
			 * The response-text of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val RESPONSE_TEXT = "resp_txt"

			/**
			 * The sender-visibility of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val SENDER_VISIBILITY = "s_vis"

			/**
			 * The reply-charging of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val REPLY_CHARGING = "r_chg"

			/**
			 * The reply-charging-deadline-token of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val REPLY_CHARGING_DEADLINE_TOKEN = "r_chg_dl_tok"

			/**
			 * The reply-charging-deadline of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val REPLY_CHARGING_DEADLINE = "r_chg_dl"

			/**
			 * The reply-charging-id of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val REPLY_CHARGING_ID = "r_chg_id"

			/**
			 * The reply-charging-size of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val REPLY_CHARGING_SIZE = "r_chg_sz"

			/**
			 * The previously-sent-by of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val PREVIOUSLY_SENT_BY = "p_s_by"

			/**
			 * The previously-sent-date of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val PREVIOUSLY_SENT_DATE = "p_s_d"

			/**
			 * The store of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val STORE = "store"

			/**
			 * The mm-state of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val MM_STATE = "mm_st"

			/**
			 * The mm-flags-token of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val MM_FLAGS_TOKEN = "mm_flg_tok"

			/**
			 * The mm-flags of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val MM_FLAGS = "mm_flg"

			/**
			 * The store-status of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val STORE_STATUS = "store_st"

			/**
			 * The store-status-text of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val STORE_STATUS_TEXT = "store_st_txt"

			/**
			 * The stored of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val STORED = "stored"

			/**
			 * The totals of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val TOTALS = "totals"

			/**
			 * The mbox-totals of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val MBOX_TOTALS = "mb_t"

			/**
			 * The mbox-totals-token of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val MBOX_TOTALS_TOKEN = "mb_t_tok"

			/**
			 * The quotas of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val QUOTAS = "qt"

			/**
			 * The mbox-quotas of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val MBOX_QUOTAS = "mb_qt"

			/**
			 * The mbox-quotas-token of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val MBOX_QUOTAS_TOKEN = "mb_qt_tok"

			/**
			 * The message-count of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val MESSAGE_COUNT = "m_cnt"

			/**
			 * The start of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val START = "start"

			/**
			 * The distribution-indicator of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val DISTRIBUTION_INDICATOR = "d_ind"

			/**
			 * The element-descriptor of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val ELEMENT_DESCRIPTOR = "e_des"

			/**
			 * The limit of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val LIMIT = "limit"

			/**
			 * The recommended-retrieval-mode of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val RECOMMENDED_RETRIEVAL_MODE = "r_r_mod"

			/**
			 * The recommended-retrieval-mode-text of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val RECOMMENDED_RETRIEVAL_MODE_TEXT = "r_r_mod_txt"

			/**
			 * The status-text of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val STATUS_TEXT = "st_txt"

			/**
			 * The applic-id of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val APPLIC_ID = "apl_id"

			/**
			 * The reply-applic-id of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val REPLY_APPLIC_ID = "r_apl_id"

			/**
			 * The aux-applic-id of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val AUX_APPLIC_ID = "aux_apl_id"

			/**
			 * The drm-content of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val DRM_CONTENT = "drm_c"

			/**
			 * The adaptation-allowed of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val ADAPTATION_ALLOWED = "adp_a"

			/**
			 * The replace-id of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val REPLACE_ID = "repl_id"

			/**
			 * The cancel-id of the message.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val CANCEL_ID = "cl_id"

			/**
			 * The cancel-status of the message.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val CANCEL_STATUS = "cl_st"

			/**
			 * The thread ID of the message
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val THREAD_ID = "thread_id"

			/**
			 * Has the message been locked?
			 * <P>
			 * Type: INTEGER (boolean)
			</P> *
			 */
			val LOCKED = "locked"

			/**
			 * Meta data used externally.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val META_DATA = "meta_data"
		}
	}

	/**
	 * Columns for the "canonical_addresses" table used by MMS and SMS."
	 */
	interface CanonicalAddressesColumns : BaseColumns {
		companion object {
			/**
			 * An address used in MMS or SMS. Email addresses are converted to lower case and are compared
			 * by string equality. Other addresses are compared using PHONE_NUMBERS_EQUAL.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val ADDRESS = "address"
		}
	}

	/**
	 * Columns for the "threads" table used by MMS and SMS.
	 */
	interface ThreadsColumns : BaseColumns {
		companion object {
			/**
			 * The date at which the thread was created.
			 *
			 * <P>
			 * Type: INTEGER (long)
			</P> *
			 */
			val DATE = "date"

			/**
			 * A string encoding of the recipient IDs of the recipients of the message, in numerical order
			 * and separated by spaces.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val RECIPIENT_IDS = "recipient_ids"

			/**
			 * The message count of the thread.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val MESSAGE_COUNT = "message_count"
			/**
			 * Indicates whether all messages of the thread have been read.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val READ = "read"

			/**
			 * The snippet of the latest message in the thread.
			 * <P>
			 * Type: TEXT
			</P> *
			 */
			val SNIPPET = "snippet"
			/**
			 * The charset of the snippet.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val SNIPPET_CHARSET = "snippet_cs"
			/**
			 * Type of the thread, either Threads.COMMON_THREAD or Threads.BROADCAST_THREAD.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val TYPE = "type"
			/**
			 * Indicates whether there is a transmission error in the thread.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val ERROR = "error"
			/**
			 * Indicates whether this thread contains any attachments.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val HAS_ATTACHMENT = "has_attachment"
		}
	}

	/**
	 * Helper functions for the "threads" table used by MMS and SMS.
	 */
	class Threads // No one should construct an instance of this class.
	private constructor() : ThreadsColumns {
		companion object {
			private val STANDARD_ENCODING = "UTF-8"
			val CONTENT_URI = Uri.withAppendedPath(
				MmsSms.CONTENT_URI, "conversations")
			val OBSOLETE_THREADS_URI = Uri.withAppendedPath(
				CONTENT_URI, "obsolete")

			val COMMON_THREAD = 0
			val BROADCAST_THREAD = 1
		}

		/**
		 * This is a single-recipient version of getOrCreateThreadId. It's convenient for use with SMS
		 * messages.
		 */
		/*public static long getOrCreateThreadId(Context context, String recipient) {
			Set<String> recipients = new HashSet<String>();

			recipients.add(recipient);
			return getOrCreateThreadId(context, recipients);
		}*/

		/**
		 * Given the recipients list and subject of an unsaved message, return its thread ID. If the
		 * message starts a new thread, allocate a new thread ID. Otherwise, use the appropriate
		 * existing thread ID.
		 *
		 * Find the thread ID of the same set of recipients (in any order, without any additions). If
		 * one is found, return it. Otherwise, return a unique thread ID.
		 */
		/*public static long getOrCreateThreadId(
				Context context, Set<String> recipients) {
			Uri.Builder uriBuilder = THREAD_ID_CONTENT_URI.buildUpon();

			for (String recipient : recipients) {
				if (Mms.isEmailAddress(recipient)) {
					recipient = Mms.extractAddrSpec(recipient);
				}

				uriBuilder.appendQueryParameter("recipient", recipient);
			}

			Uri uri = uriBuilder.build();
			// if (DEBUG) Log.v(TAG, "getOrCreateThreadId uri: " + uri);

			Cursor cursor = SqliteWrapper.query(context, context.getContentResolver(),
					uri, ID_PROJECTION, null, null, null);
			if (DEBUG) {
				Log.v(TAG, "getOrCreateThreadId cursor cnt: " + cursor.getCount());
			}
			if (cursor != null) {
				try {
					if (cursor.moveToFirst()) {
						return cursor.getLong(0);
					} else {
						Log.e(TAG, "getOrCreateThreadId returned no rows!");
					}
				} finally {
					cursor.close();
				}
			}

			Log.e(TAG, "getOrCreateThreadId failed with uri " + uri.toString());
			throw new IllegalArgumentException("Unable to find or allocate a thread ID.");
		}*/
	}

	/**
	 * Contains all MMS messages.
	 */
	class Mms : BaseMmsColumns {

		/**
		 * Contains all MMS messages in the MMS app's inbox.
		 */
		class Inbox : BaseMmsColumns {
			companion object {
				/**
				 * The content:// style URL for this table
				 */
				val CONTENT_URI = Uri.parse("content://mms/inbox")

				/**
				 * The default sort order for this table
				 */
				val DEFAULT_SORT_ORDER = "date DESC"
			}
		}

		/**
		 * Contains all MMS messages in the MMS app's sent box.
		 */
		class Sent : BaseMmsColumns {
			companion object {
				/**
				 * The content:// style URL for this table
				 */
				val CONTENT_URI = Uri.parse("content://mms/sent")

				/**
				 * The default sort order for this table
				 */
				val DEFAULT_SORT_ORDER = "date DESC"
			}
		}

		/**
		 * Contains all MMS messages in the MMS app's drafts box.
		 */
		class Draft : BaseMmsColumns {
			companion object {
				/**
				 * The content:// style URL for this table
				 */
				val CONTENT_URI = Uri.parse("content://mms/drafts")

				/**
				 * The default sort order for this table
				 */
				val DEFAULT_SORT_ORDER = "date DESC"
			}
		}

		/**
		 * Contains all MMS messages in the MMS app's outbox.
		 */
		class Outbox : BaseMmsColumns {
			companion object {
				/**
				 * The content:// style URL for this table
				 */
				val CONTENT_URI = Uri.parse("content://mms/outbox")

				/**
				 * The default sort order for this table
				 */
				val DEFAULT_SORT_ORDER = "date DESC"
			}
		}

		class Addr : BaseColumns {
			companion object {
				/**
				 * The ID of MM which this address entry belongs to.
				 */
				val MSG_ID = "msg_id"

				/**
				 * The ID of contact entry in Phone Book.
				 */
				val CONTACT_ID = "contact_id"

				/**
				 * The address text.
				 */
				val ADDRESS = "address"

				/**
				 * Type of address, must be one of PduHeaders.BCC, PduHeaders.CC, PduHeaders.FROM,
				 * PduHeaders.TO.
				 */
				val TYPE = "type"

				/**
				 * Character set of this entry.
				 */
				val CHARSET = "charset"
			}
		}

		class Part : BaseColumns {
			companion object {
				/**
				 * The identifier of the message which this part belongs to.
				 * <P>
				 * Type: INTEGER
				</P> *
				 */
				val MSG_ID = "mid"

				/**
				 * The order of the part.
				 * <P>
				 * Type: INTEGER
				</P> *
				 */
				val SEQ = "seq"

				/**
				 * The content type of the part.
				 * <P>
				 * Type: TEXT
				</P> *
				 */
				val CONTENT_TYPE = "ct"

				/**
				 * The name of the part.
				 * <P>
				 * Type: TEXT
				</P> *
				 */
				val NAME = "name"

				/**
				 * The charset of the part.
				 * <P>
				 * Type: TEXT
				</P> *
				 */
				val CHARSET = "chset"

				/**
				 * The file name of the part.
				 * <P>
				 * Type: TEXT
				</P> *
				 */
				val FILENAME = "fn"

				/**
				 * The content disposition of the part.
				 * <P>
				 * Type: TEXT
				</P> *
				 */
				val CONTENT_DISPOSITION = "cd"

				/**
				 * The content ID of the part.
				 * <P>
				 * Type: INTEGER
				</P> *
				 */
				val CONTENT_ID = "cid"

				/**
				 * The content location of the part.
				 * <P>
				 * Type: INTEGER
				</P> *
				 */
				val CONTENT_LOCATION = "cl"

				/**
				 * The start of content-type of the message.
				 * <P>
				 * Type: INTEGER
				</P> *
				 */
				val CT_START = "ctt_s"

				/**
				 * The type of content-type of the message.
				 * <P>
				 * Type: TEXT
				</P> *
				 */
				val CT_TYPE = "ctt_t"

				/**
				 * The location(on filesystem) of the binary data of the part.
				 * <P>
				 * Type: INTEGER
				</P> *
				 */
				val _DATA = "_data"

				val TEXT = "text"
			}

		}

		object Rate {
			val CONTENT_URI = Uri.withAppendedPath(
				Companion.CONTENT_URI, "rate")
			/**
			 * When a message was successfully sent.
			 * <P>
			 * Type: INTEGER
			</P> *
			 */
			val SENT_TIME = "sent_time"
		}

		object ScrapSpace {
			/**
			 * The content:// style URL for this table
			 */
			val CONTENT_URI = Uri.parse("content://mms/scrapSpace")

			/**
			 * This is the scrap file we use to store the media attachment when the user chooses to
			 * capture a photo to be attached . We pass {#link@Uri} to the Camera app, which streams
			 * the captured image to the uri. Internally we write the media content to this file. It's
			 * named '.temp.jpg' so Gallery won't pick it up.
			 */
			val SCRAP_FILE_PATH = Environment.getExternalStorageDirectory().path + "/mms/scrapSpace/.temp.jpg"
		}

		object Intents {

			/**
			 * The extra field to store the contents of the Intent, which should be an array of Uri.
			 */
			val EXTRA_CONTENTS = "contents"
			/**
			 * The extra field to store the type of the contents, which should be an array of String.
			 */
			val EXTRA_TYPES = "types"
			/**
			 * The extra field to store the 'Cc' addresses.
			 */
			val EXTRA_CC = "cc"
			/**
			 * The extra field to store the 'Bcc' addresses;
			 */
			val EXTRA_BCC = "bcc"
			/**
			 * The extra field to store the 'Subject'.
			 */
			val EXTRA_SUBJECT = "subject"
			/**
			 * Indicates that the contents of specified URIs were changed. The application which is
			 * showing or caching these contents should be updated.
			 */
			val CONTENT_CHANGED_ACTION = "android.intent.action.CONTENT_CHANGED"
			/**
			 * An extra field which stores the URI of deleted contents.
			 */
			val DELETED_CONTENTS = "deleted_contents"
		} // Non-instantiatable.

		companion object {
			/**
			 * The content:// style URL for this table
			 */
			val CONTENT_URI = Uri.parse("content://mms")

			val REPORT_REQUEST_URI = Uri.withAppendedPath(
				CONTENT_URI, "report-request")

			val REPORT_STATUS_URI = Uri.withAppendedPath(
				CONTENT_URI, "report-status")

			/**
			 * The default sort order for this table
			 */
			val DEFAULT_SORT_ORDER = "date DESC"

			/**
			 * mailbox = name-addr name-addr = [display-name] angle-addr angle-addr = [CFWS] "<" addr-spec
			 * ">" [CFWS]
			 */
			val NAME_ADDR_EMAIL_PATTERN = Pattern.compile("\\s*(\"[^\"]*\"|[^<>\"]+)\\s*<([^<>]+)>\\s*")

			/**
			 * quoted-string = [CFWS] DQUOTE *([FWS] qcontent) [FWS] DQUOTE [CFWS]
			 */
			val QUOTED_STRING_PATTERN = Pattern.compile("\\s*\"([^\"]*)\"\\s*")

			fun query(
				cr: ContentResolver, projection: Array<String>): Cursor? {
				return cr.query(CONTENT_URI, projection, null, null, DEFAULT_SORT_ORDER)
			}

			fun query(
				cr: ContentResolver, projection: Array<String>,
				where: String, orderBy: String?): Cursor? {
				return cr.query(CONTENT_URI, projection,
					where, null, orderBy ?: DEFAULT_SORT_ORDER)
			}

			fun getMessageBoxName(msgBox: Int): String {
				when (msgBox) {
					BaseMmsColumns.MESSAGE_BOX_ALL -> return "all"
					BaseMmsColumns.MESSAGE_BOX_INBOX -> return "inbox"
					BaseMmsColumns.MESSAGE_BOX_SENT -> return "sent"
					BaseMmsColumns.MESSAGE_BOX_DRAFTS -> return "drafts"
					BaseMmsColumns.MESSAGE_BOX_OUTBOX -> return "outbox"
					else -> throw IllegalArgumentException("Invalid message box: $msgBox")
				}
			}

			fun extractAddrSpec(address: String): String {
				val match = NAME_ADDR_EMAIL_PATTERN.matcher(address)

				return if (match.matches()) {
					match.group(2)
				} else address
			}

			/**
			 * Returns true if the address is an email address
			 *
			 * @param address
			 * the input address to be tested
			 * @return true if address is an email address
			 */
			fun isEmailAddress(address: String): Boolean {
				if (TextUtils.isEmpty(address)) {
					return false
				}

				val s = extractAddrSpec(address)
				val match = Patterns.EMAIL_ADDRESS.matcher(s)
				return match.matches()
			}

			/**
			 * Returns true if the number is a Phone number
			 *
			 * @param number
			 * the input number to be tested
			 * @return true if number is a Phone number
			 */
			fun isPhoneNumber(number: String): Boolean {
				if (TextUtils.isEmpty(number)) {
					return false
				}

				val match = Patterns.PHONE.matcher(number)
				return match.matches()
			}
		}
	}

	/**
	 * Contains all MMS and SMS messages.
	 */
	class MmsSms : BaseColumns {

		class PendingMessages : BaseColumns {
			companion object {
				val CONTENT_URI = Uri.withAppendedPath(
					MmsSms.CONTENT_URI, "pending")
				/**
				 * The type of transport protocol(MMS or SMS).
				 * <P>
				 * Type: INTEGER
				</P> *
				 */
				val PROTO_TYPE = "proto_type"
				/**
				 * The ID of the message to be sent or downloaded.
				 * <P>
				 * Type: INTEGER
				</P> *
				 */
				val MSG_ID = "msg_id"
				/**
				 * The type of the message to be sent or downloaded. This field is only valid for MM. For
				 * SM, its value is always set to 0.
				 */
				val MSG_TYPE = "msg_type"
				/**
				 * The type of the error code.
				 * <P>
				 * Type: INTEGER
				</P> *
				 */
				val ERROR_TYPE = "err_type"
				/**
				 * The error code of sending/retrieving process.
				 * <P>
				 * Type: INTEGER
				</P> *
				 */
				val ERROR_CODE = "err_code"
				/**
				 * How many times we tried to send or download the message.
				 * <P>
				 * Type: INTEGER
				</P> *
				 */
				val RETRY_INDEX = "retry_index"
				/**
				 * The time to do next retry.
				 */
				val DUE_TIME = "due_time"
				/**
				 * The time we last tried to send or download the message.
				 */
				val LAST_TRY = "last_try"
			}
		}

		object WordsTable {
			val ID = "_id"
			val SOURCE_ROW_ID = "source_id"
			val TABLE_ID = "table_to_use"
			val INDEXED_TEXT = "index_text"
		}

		companion object {
			/**
			 * The column to distinguish SMS &amp; MMS messages in query results.
			 */
			val TYPE_DISCRIMINATOR_COLUMN = "transport_type"

			val CONTENT_URI = Uri.parse("content://mms-sms/")

			val CONTENT_CONVERSATIONS_URI = Uri.parse(
				"content://mms-sms/conversations")

			val CONTENT_FILTER_BYPHONE_URI = Uri.parse(
				"content://mms-sms/messages/byphone")

			val CONTENT_UNDELIVERED_URI = Uri.parse(
				"content://mms-sms/undelivered")

			val CONTENT_DRAFT_URI = Uri.parse(
				"content://mms-sms/draft")

			val CONTENT_LOCKED_URI = Uri.parse(
				"content://mms-sms/locked")

			/***
			 * Pass in a query parameter called "pattern" which is the text to search for. The sort order
			 * is fixed to be thread_id ASC,date DESC.
			 */
			val SEARCH_URI = Uri.parse(
				"content://mms-sms/search")

			// Constants for message protocol types.
			val SMS_PROTO = 0
			val MMS_PROTO = 1

			// Constants for error types of pending messages.
			val NO_ERROR = 0
			val ERR_TYPE_GENERIC = 1
			val ERR_TYPE_SMS_PROTO_TRANSIENT = 2
			val ERR_TYPE_MMS_PROTO_TRANSIENT = 3
			val ERR_TYPE_TRANSPORT_FAILURE = 4
			val ERR_TYPE_GENERIC_PERMANENT = 10
			val ERR_TYPE_SMS_PROTO_PERMANENT = 11
			val ERR_TYPE_MMS_PROTO_PERMANENT = 12
		}
	}

	class Carriers : BaseColumns {
		companion object {
			/**
			 * The content:// style URL for this table
			 */
			val CONTENT_URI = Uri.parse("content://telephony/carriers")

			/**
			 * The default sort order for this table
			 */
			val DEFAULT_SORT_ORDER = "name ASC"

			val NAME = "name"

			val APN = "apn"

			val PROXY = "proxy"

			val PORT = "port"

			val MMSPROXY = "mmsproxy"

			val MMSPORT = "mmsport"

			val SERVER = "server"

			val USER = "user"

			val PASSWORD = "password"

			val MMSC = "mmsc"

			val MCC = "mcc"

			val MNC = "mnc"

			val NUMERIC = "numeric"

			val AUTH_TYPE = "authtype"

			val TYPE = "type"

			/**
			 * The protocol to be used to connect to this APN.
			 *
			 * One of the PDP_type values in TS 27.007 section 10.1.1. For example, "IP", "IPV6",
			 * "IPV4V6", or "PPP".
			 */
			val PROTOCOL = "protocol"

			/**
			 * The protocol to be used to connect to this APN when roaming.
			 *
			 * The syntax is the same as protocol.
			 */
			val ROAMING_PROTOCOL = "roaming_protocol"

			val CURRENT = "current"
		}
	}

	object Intents {

		/**
		 * Broadcast Action: A "secret code" has been entered in the dialer. Secret codes are of the
		 * form *#*#`#*#*. The intent will have the data URI:
		 *
		 *
		 * `android_secret_code://<code>`
		` */
		val SECRET_CODE_ACTION = "android.provider.Telephony.SECRET_CODE"

		/**
		 * Broadcast Action: The Service Provider string(s) have been updated. Activities or services
		 * that use these strings should update their display. The intent will have the following
		 * extra values:
		 *
		 *  * *showPlmn* - Boolean that indicates whether the PLMN should be shown.
		 *  * *plmn* - The operator name of the registered network, as a string.
		 *  * *showSpn* - Boolean that indicates whether the SPN should be shown.
		 *  * *spn* - The service provider name, as a string.
		 *
		 * Note that *showPlmn* may indicate that *plmn* should be displayed, even
		 * though the value for *plmn* is null. This can happen, for example, if the phone has
		 * not registered to a network yet. In this case the receiver may substitute an appropriate
		 * placeholder string (eg, "No service").
		 *
		 * It is recommended to display *plmn* before / above *spn* if both are
		 * displayed.
		 *
		 *
		 *
		 * Note this is a protected intent that can only be sent by the system.
		 */
		val SPN_STRINGS_UPDATED_ACTION = "android.provider.Telephony.SPN_STRINGS_UPDATED"

		val EXTRA_SHOW_PLMN = "showPlmn"
		val EXTRA_PLMN = "plmn"
		val EXTRA_SHOW_SPN = "showSpn"
		val EXTRA_SPN = "spn"
	} // Not instantiable
}